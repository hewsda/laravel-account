<?php

declare(strict_types=1);

namespace Hewsda\Account\Infrastructure\Repository\Account\Rolable;

use Hewsda\Account\Model\Account\LocalAccount\Event\RoleWasAttached;
use Hewsda\Account\Model\Account\LocalAccount\Event\RoleWasGiven;
use Hewsda\LaravelModelEvent\ModelChanged;
use Illuminate\Database\Connection;

class RolableStore
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * RolableStore constructor.
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function attachRole(RoleWasAttached $event): void
    {
        $this->connection->table('account_role')->insert([
            'id' => $event->aggregateId(),
            'account_id' => $event->accountId()->toString(),
            'role_id' => $event->roleId()
        ]);
    }

    public function giveRole(RoleWasGiven $event): void
    {
        $this->connection->table('account_role')->insert([
            'id' => $event->aggregateId(),
            'account_id' => $event->accountId()->toString(),
            'role_id' => $event->roleId(),
            'assigner_id' => $event->assignerId()->toString()
        ]);
    }

    public function onEvent(ModelChanged $event): void
    {
        switch ($event) {
            case $event instanceof RoleWasAttached:
                $this->attachRole($event);
                break;
            case $event instanceof RoleWasGiven:
                $this->giveRole($event);
                break;
        }
    }
}