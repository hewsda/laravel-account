<?php

declare(strict_types=1);

namespace Hewsda\Account\Infrastructure\Repository\Account\Rolable;

use Hewsda\Account\Model\Account\Rolable\Repository\RolableRepository as RolableRepositoryContract;
use Hewsda\Account\Model\Account\Rolable\Rolable;
use Hewsda\LaravelModelEvent\Model\Repository\ModelRepository;

class RolableRepository extends ModelRepository implements RolableRepositoryContract
{
    public function save(Rolable $rolable): void
    {
        $this->saveAggregateRoot($rolable);
    }
}