<?php

declare(strict_types=1);

namespace Hewsda\Account\Infrastructure\Repository\Account\PasswordReset;

use Hewsda\Account\Model\Account\LocalAccount\Event\LocalPasswordWasChanged;
use Hewsda\Account\Model\Account\PasswordReset\Event\PasswordResetWasCreated;
use Hewsda\Account\Model\Account\PasswordReset\Event\PasswordResetWasDeleted;
use Hewsda\LaravelModelEvent\ModelChanged;
use Illuminate\Database\Connection;
use Illuminate\Database\Query\Builder;

class PasswordResetStore
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * PasswordResetStore constructor.
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function create(PasswordResetWasCreated $event): void
    {
        $this->connection->table('password_resets')
            ->insert([
                'id' => $event->aggregateId(),
                'account_id' => $event->accountId()->toString(),
                'code' => $event->code()->toString(),
                'created_at' => $event->createdAt()->format('Y-m-d H:i:s')
            ]);
    }

    public function delete(PasswordResetWasDeleted $event): void
    {
        $this->connection->table('password_resets')
            ->whereExists(function (Builder $q) use ($event) {
                $q
                    ->where('code', $event->code()->toString())
                    ->where('account_id', $event->accountId()->toString());
            })->delete($event->aggregateId());
    }

    public function deleteAllFromAccount(LocalPasswordWasChanged $event): void
    {
        $this->connection->table('password_resets')
            ->whereExists(function (Builder $q) use ($event) {
                $q->where('account_id', $event->accountId()->toString());
            })->delete();
    }

    public function onEvent(ModelChanged $event): void
    {
        switch ($event) {

            case $event instanceof PasswordResetWasCreated:
                $this->create($event);
                break;

            case $event instanceof LocalPasswordWasChanged:
                $this->deleteAllFromAccount($event);
                break;

            case $event instanceof PasswordResetWasDeleted:
                $this->delete($event);
                break;

            default:
                throw new \InvalidArgumentException(
                    sprintf('Missing event handler %s for class %s', get_class($event), static::class)
                );
        }
    }
}