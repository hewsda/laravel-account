<?php

declare(strict_types=1);

namespace Hewsda\Account\Infrastructure\Repository\Account\PasswordReset;

use Hewsda\Account\Infrastructure\Repository\Account\Local\LocalAccountRepository;
use Hewsda\Account\Model\Account\PasswordReset\PasswordReset;
use Hewsda\Account\Model\Account\PasswordReset\Repository\PasswordResetRepository as RepositoryContract;
use Hewsda\LaravelModelEvent\Model\Repository\ModelRepository;
use Hewsda\Security\Foundation\Contracts\Core\User\User;
use Illuminate\Database\Eloquent\Builder;
use Prooph\ServiceBus\EventBus;

class PasswordResetRepository extends ModelRepository implements RepositoryContract
{
    /**
     * @var LocalAccountRepository
     */
    private $accountRepository;

    /**
     * @var PasswordReset
     */
    private $model;

    /**
     * PasswordResetRepository constructor.
     *
     * @param PasswordReset $model
     * @param LocalAccountRepository $accountRepository
     * @param EventBus $eventBus
     */
    public function __construct(PasswordReset $model, LocalAccountRepository $accountRepository, EventBus $eventBus)
    {
        parent::__construct($eventBus);

        $this->model = $model;
        $this->accountRepository = $accountRepository;
    }

    public function save(PasswordReset $aggregate): void
    {
        $this->saveAggregateRoot($aggregate);
    }

    public function accountOfPasswordResetCode(string $code): ?User
    {
        return $this->accountRepository->accountOfPasswordResetCode($code);
    }

    public function get(string $rootId): ?PasswordReset
    {
        return $this->createModel()->find($rootId);
    }

    public function passwordResetOfCode(string $passwordResetCode): ?PasswordReset
    {
        return $this->createModel()->where('code', $passwordResetCode)->first();
    }

    public function createModel(): Builder
    {
        return $this->model->newInstance()->newQuery();
    }
}