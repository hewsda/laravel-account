<?php

declare(strict_types=1);

namespace Hewsda\Account\Infrastructure\Repository\Account\Activation;

use Hewsda\Account\Infrastructure\Repository\Account\Local\LocalAccountRepository;
use Hewsda\Account\Model\Account\Activation\Activation;
use Hewsda\Account\Model\Account\Activation\Repository\ActivationRepository as BaseRepository;
use Hewsda\LaravelModelEvent\Model\Repository\ModelRepository;
use Hewsda\Security\Foundation\Contracts\Core\User\User;
use Prooph\ServiceBus\EventBus;

class ActivationRepository extends ModelRepository implements BaseRepository
{
    /**
     * @var LocalAccountRepository
     */
    private $repository;

    /**
     * ActivationRepository constructor.
     *
     * @param LocalAccountRepository $repository
     * @param EventBus $eventBus
     */
    public function __construct(LocalAccountRepository $repository, EventBus $eventBus)
    {
        parent::__construct($eventBus);

        $this->repository = $repository;
    }

    public function accountOfActivationCode(string $code): ?User
    {
        return $this->repository->accountOfActivationCode($code);
    }

    public function save(Activation $root): void
    {
        $this->saveAggregateRoot($root);
    }
}
