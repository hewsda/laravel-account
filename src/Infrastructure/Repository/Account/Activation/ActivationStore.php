<?php

declare(strict_types=1);

namespace Hewsda\Account\Infrastructure\Repository\Account\Activation;

use Hewsda\Account\Model\Account\Activation\Event\NotActivatedAccountWasCreated;
use Hewsda\Account\Model\Account\LocalAccount\Event\AccountWasActivated;
use Hewsda\LaravelModelEvent\ModelChanged;
use Illuminate\Database\Connection;
use Illuminate\Database\Query\Builder;

class ActivationStore
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * ActivationStore constructor.
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function create(NotActivatedAccountWasCreated $event): void
    {
        $this->connection->table('account_activation')
            ->insert([
                'id' => $event->id()->toString(),
                'account_id' => $event->accountId()->toString(),
                'activation_code' => $event->activationCode()->code(),
                'created_at' => $event->createdAt()->format('Y-m-d H:i:s')
            ]);
    }

    public function delete(AccountWasActivated $event): void
    {
        $accountId = $event->accountId()->toString();

        $this->connection->table('account_activation')
            ->whereExists(function (Builder $q) use ($accountId) {
                $q->where('id', $accountId);
            })->delete($accountId);
    }

    public function onEvent(ModelChanged $event): void
    {
        switch ($event) {
            case $event instanceof NotActivatedAccountWasCreated:
                $this->create($event);
                break;
            case $event instanceof AccountWasActivated:
                $this->delete($event);
                break;
            default:
                throw new \RuntimeException(
                    sprintf('Event %s was not handled by %s', get_class($event), static::class)

                );
        }
    }
}