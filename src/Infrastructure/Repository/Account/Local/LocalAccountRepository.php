<?php

declare(strict_types=1);

namespace Hewsda\Account\Infrastructure\Repository\Account\Local;

use Hewsda\Account\Infrastructure\Repository\Account\AccountRepository;
use Hewsda\Account\Model\Account\Activation\Repository\ActivationRepository;
use Hewsda\Account\Model\Account\LocalAccount\LocalAccount;
use Hewsda\Account\Model\Account\LocalAccount\Repository\LocalAccountRepositoryContract;
use Hewsda\Account\Model\Account\PasswordReset\Repository\PasswordResetRepository;
use Hewsda\Security\Foundation\Contracts\Core\User\ExtendedUser as SecurityUser;
use Hewsda\Security\Foundation\Contracts\Core\User\User;
use Illuminate\Database\Eloquent\Builder;
use Prooph\ServiceBus\EventBus;

class LocalAccountRepository extends AccountRepository implements   LocalAccountRepositoryContract,
                                                                    ActivationRepository,
                                                                    PasswordResetRepository
{
    /**
     * @var LocalAccount
     */
    private $model;

    /**
     * LocalAccountRepository constructor.
     *
     * @param LocalAccount $model
     * @param EventBus $eventBus
     */
    public function __construct(LocalAccount $model, EventBus $eventBus)
    {
        parent::__construct($eventBus);

        $this->model = $model;
    }

    public function accountOfId(string $userId): ?SecurityUser
    {
        return $this->createModel()->find($userId);
    }

    public function accountOfEmail(string $email): ?SecurityUser
    {
        return $this->createModel()->where('email', $email)->first();
    }

    public function accountOfActivationCode(string $code): ?User
    {
        return $this->createModel()
            ->where('activated', 0)
            ->with('activation')
            ->whereHas('activation', function (Builder $q) use ($code) {
                $q->where('activation_code', $code);
            })->first();
    }

    public function accountOfPasswordResetCode(string $code): ?User
    {
        return $this->createModel()
            ->with('uniquePasswordReset')
            ->whereHas('uniquePasswordReset', function (Builder $q) use ($code) {
                $q
                    ->isNotExpired()
                    ->where('code', $code);
            })->first();
    }

    public function save(LocalAccount $account): void
    {
        $this->saveAggregateRoot($account);
    }

    public function supportsClass(string $class): bool
    {
        return $class === LocalAccount::class;
    }

    public function createModel(): Builder
    {
        return $this->model->newInstance()->newQuery();
    }
}