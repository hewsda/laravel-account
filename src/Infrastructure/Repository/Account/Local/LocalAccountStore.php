<?php

declare(strict_types=1);

namespace Hewsda\Account\Infrastructure\Repository\Account\Local;

use Hewsda\Account\Model\Account\LocalAccount\Event\AccountWasActivated;
use Hewsda\Account\Model\Account\LocalAccount\Event\LocalAccountWasCreated;
use Hewsda\Account\Model\Account\LocalAccount\Event\LocalAccountWasRegistered;
use Hewsda\Account\Model\Account\LocalAccount\Event\LocalEmailWasChanged;
use Hewsda\Account\Model\Account\LocalAccount\Event\LocalPasswordWasChanged;
use Hewsda\Account\Model\Account\LocalAccount\Event\LocalPasswordWasReset;
use Hewsda\LaravelModelEvent\ModelChanged;
use Illuminate\Database\Connection;

class LocalAccountStore
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * LocalAccountStore constructor.
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function registerAccount(LocalAccountWasRegistered $event): void
    {
        $this->connection->table('local_accounts')->insert([
            'id' => $event->accountId()->toString(),
            'email' => $event->email()->toString(),
            'name' => $event->name(),
            'password' => $event->password()->toString(),
            'created_at' => $this->formatDate($event->createdAt())
        ]);
    }

    public function createAccount(LocalAccountWasCreated $event): void
    {
        $this->connection->table('local_accounts')->insert([
            'id' => $event->accountId()->toString(),
            'email' => $event->email()->toString(),
            'name' => $event->name(),
            'password' => $event->password()->toString(),
            'created_at' => $this->formatDate($event->createdAt())
        ]);
    }

    public function changePassword(LocalPasswordWasChanged $event): void
    {
        $this->connection->table('local_accounts')
            ->where('id', $event->accountId()->toString())
            ->update([
                'password' => $event->newPassword()->toString(),
                'updated_at' => $this->formatDate($event->createdAt())
            ]);
    }

    public function resetPassword(LocalPasswordWasReset $event): void
    {
        $this->connection->table('local_accounts')
            ->where('id', $event->accountId()->toString())
            ->update([
                'password' => $event->newPassword()->toString(),
                'updated_at' => $this->formatDate($event->createdAt())
            ]);
    }

    public function changeEmail(LocalEmailWasChanged $event): void
    {
        $this->connection->table('local_accounts')
            ->where('id', $event->accountId()->toString())
            ->update([
                'email' => $event->newEmail()->toString(),
                'updated_at' => $this->formatDate($event->createdAt())
            ]);
    }

    public function activateAccount(AccountWasActivated $event): void
    {
        $this->connection->table('local_accounts')
            ->where('id', $event->accountId()->toString())
            ->update([
                'activated' => $event->activated()->getValue(),
                'updated_at' => $this->formatDate($event->createdAt())
            ]);
    }

    public function onEvent(ModelChanged $event): void
    {
        switch ($event) {
            case $event instanceof LocalAccountWasRegistered:
                $this->registerAccount($event);
                break;
            case $event instanceof LocalAccountWasCreated:
                $this->createAccount($event);
                break;
            case $event instanceof LocalPasswordWasChanged:
                $this->changePassword($event);
                break;
            case $event instanceof LocalEmailWasChanged:
                $this->changeEmail($event);
                break;
            case $event instanceof AccountWasActivated:
                $this->activateAccount($event);
                break;
            case $event instanceof LocalPasswordWasReset:
                $this->resetPassword($event);
                break;
            default:
                throw new \RuntimeException(
                    sprintf('Missing event handler %s for class %s', get_class($event), static::class)
                );
        }
    }

    protected function formatDate(\DateTimeImmutable $dateTimeImmutable): string
    {
        return $dateTimeImmutable->format('Y-m-d H:i:s');
    }
}