<?php

declare(strict_types=1);

namespace Hewsda\Account\Infrastructure\Repository\Account;

use Hewsda\Account\Model\Account\Repository\AccountRepository as RepositoryContract;
use Hewsda\LaravelModelEvent\Model\Repository\ModelRepository;
use Hewsda\Security\Core\Exception\UnsupportedUser;
use Hewsda\Security\Core\Exception\UserNotFound;
use Hewsda\Security\Foundation\Contracts\Core\User\Identifier;
use Hewsda\Security\Foundation\Contracts\Core\User\User as UserContract;

abstract class AccountRepository extends ModelRepository implements RepositoryContract
{
    use AccountIdentifier;

    public function requireByIdentifier(Identifier $identifier): UserContract
    {
        if (null !== $user = $this->accountOfIdentifier($identifier)) {
            return $user;
        }

        throw new UserNotFound('User not found');
    }

    public function refreshUser(UserContract $user): UserContract
    {
        if (!$this->supportsClass(get_class($user))) {
            throw new UnsupportedUser(
                sprintf('User class %s is not supported.', get_class($user)));
        }

        return $this->requireByIdentifier($user->getIdentifier());
    }
}