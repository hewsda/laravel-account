<?php

declare(strict_types=1);

namespace Hewsda\Account\Infrastructure\Repository\Account;

use Hewsda\Account\Application\Security\AccountActivation\ActivationCode;
use Hewsda\Account\Model\Account\PasswordReset\PasswordResetCode;
use Hewsda\Security\Foundation\Contracts\Core\User\ExtendedUser as SecurityUser;
use Hewsda\Security\Foundation\Contracts\Core\User\Identifier;
use Hewsda\Security\Foundation\Value\EmailAddress;
use Hewsda\Security\Foundation\Value\UserId;

trait AccountIdentifier
{
    public function accountOfIdentifier(Identifier $identifier): ?SecurityUser
    {
        if ($identifier instanceof EmailAddress) {
            return $this->accountOfEmail($identifier->toString());
        }

        if ($identifier instanceof UserId && method_exists($this, 'userOfId')) {
            return $this->accountOfId($identifier->toString());
        }

        if ($identifier instanceof ActivationCode && method_exists($this, 'accountOfActivationCode')) {
            return $this->accountOfActivationCode($identifier->code());
        }

        if ($identifier instanceof PasswordResetCode && method_exists($this, 'accountOfPasswordResetCode')) {
            return $this->accountOfPasswordResetCode($identifier->code());
        }

        throw new \InvalidArgumentException('Identifier is an unknown type.');
    }
}