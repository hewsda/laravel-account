<?php

declare(strict_types=1);

namespace Hewsda\Account\Infrastructure\Repository\Role;

use Hewsda\Account\Model\Role\Repository\RoleList;
use Hewsda\Account\Model\Role\Role;
use Hewsda\LaravelModelEvent\Model\Repository\ModelRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Prooph\ServiceBus\EventBus;

class RoleCollection extends ModelRepository implements RoleList
{
    /**
     * @var Role|Model
     */
    private $role;

    /**
     * RoleCollection constructor.
     *
     * @param Role $role
     * @param EventBus $eventBus
     */
    public function __construct(Role $role, EventBus $eventBus)
    {
        $this->role = $role;

        parent::__construct($eventBus);
    }

    public function save(Role $role): void
    {
        $this->saveAggregateRoot($role);
    }

    public function roleOfId(string $roleId): ?Role
    {
        return $this->newModel()->find($roleId);
    }

    public function roleOfName(string $roleName): ?Role
    {
        return $this->newModel()->where('name', $roleName)->first();
    }

    public function has(string $roleName): bool
    {
        return null !== $this->roleOfName($roleName);
    }

    public function newModel(): Builder
    {
        return $this->role->newInstance()->newQuery();
    }
}