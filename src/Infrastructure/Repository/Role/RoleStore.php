<?php

declare(strict_types=1);

namespace Hewsda\Account\Infrastructure\Repository\Role;

use Hewsda\Account\Model\Role\Event\RoleWasCreated;
use Hewsda\LaravelModelEvent\ModelChanged;
use Illuminate\Database\Connection;

class RoleStore
{

    /**
     * @var Connection
     */
    private $connection;

    /**
     * RoleStore constructor.
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function createRole(RoleWasCreated $event)
    {
        $this->connection->table('roles')->insert([
            'id' => $event->aggregateId(),
            'name' => $event->name(),
            'slug' => $event->slug(),
            'description' => $event->description()
        ]);
    }

    public function onEvent(ModelChanged $event): void
    {
        switch ($event){
            case $event instanceof RoleWasCreated:
                $this->createRole($event);
                break;
        }
    }
}