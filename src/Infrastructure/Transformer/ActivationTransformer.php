<?php

declare(strict_types=1);

namespace Hewsda\Account\Infrastructure\Transformer;

use Hewsda\Account\Model\Account\Activation\Activation;
use League\Fractal\TransformerAbstract;

class ActivationTransformer extends TransformerAbstract
{
    public function transform(Activation $activation): array
    {
        return [
            'id' => $activation->getKey(),
            'account_id' => $activation->accountId()->toString(),
            'activation_code' => $activation->activationCode()->code(),
            'created_at' => $activation->createdAt()
        ];
    }
}