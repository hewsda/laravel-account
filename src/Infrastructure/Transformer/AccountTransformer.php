<?php

declare(strict_types=1);

namespace Hewsda\Account\Infrastructure\Transformer;

use Hewsda\Account\Model\Account\LocalAccount\LocalAccount;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\ResourceInterface;
use League\Fractal\TransformerAbstract;

class AccountTransformer extends TransformerAbstract
{

    public function getDefaultIncludes(): array
    {
        return ['roles', 'activation'];
    }

    public function transform(LocalAccount $account): array
    {
        return [
            'id' => $account->getId()->toString(),
            'email' => $account->getEmail()->toString(),
            'name' => $account->getDisplayableName(),
            'activated' => (int)$account->getActivated()->getValue(),
            'created_at' => $account->createdAt(),
            'updated_at' => $account->updatedAt()
        ];
    }

    public function includeRoles(LocalAccount $account): Collection
    {
        return $this->collection($account->roles, new RoleTransformer());
    }

    public function includeActivation(LocalAccount $account): ResourceInterface
    {
        if ($account->isActivated()) {
            return $this->null();
        }

        return $this->item($account->activation, new ActivationTransformer());
    }
}