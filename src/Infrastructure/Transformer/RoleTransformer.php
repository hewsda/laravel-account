<?php

declare(strict_types=1);

namespace Hewsda\Account\Infrastructure\Transformer;

use Hewsda\Account\Model\Role\Role;
use League\Fractal\TransformerAbstract;

class RoleTransformer extends TransformerAbstract
{
    public function transform(Role $role): array
    {
        return [
            'id' => $role->getId(),
            'name' => $role->getRole(),
            'slug' => $role->getSlug(),
            'description' => $role->getDescription()
        ];
    }
}