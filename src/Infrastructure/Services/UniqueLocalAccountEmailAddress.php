<?php

declare(strict_types=1);

namespace Hewsda\Account\Infrastructure\Services;

use Hewsda\Account\Application\Values\Account\AccountEmail;
use Hewsda\Account\Application\Values\Account\LocalAccountId;
use Hewsda\Account\Infrastructure\Repository\Account\Local\LocalAccountRepository;
use Hewsda\Account\Model\Account\LocalAccount\Services\CheckUniqueLocalEmailAddress;

class UniqueLocalAccountEmailAddress implements CheckUniqueLocalEmailAddress
{
    /**
     * @var LocalAccountRepository
     */
    private $repository;

    /**
     * UniqueLocalAccountEmailAddress constructor.
     *
     * @param LocalAccountRepository $repository
     */
    public function __construct(LocalAccountRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(AccountEmail $email): ?LocalAccountId
    {
        if (null !== $account = $this->repository->accountOfEmail($email->toString())) {
            return $account->getId();
        }

        return null;
    }
}