<?php

declare(strict_types=1);

namespace Hewsda\Account\Infrastructure\Services;

use Hewsda\Account\Application\Values\Account\EncodedPassword;
use Hewsda\Account\Application\Values\Account\LocalPassword;
use Hewsda\Account\Model\Account\LocalAccount\Services\LocalPasswordEncoder;
use Illuminate\Contracts\Hashing\Hasher;


class BcryptPasswordEncoder implements LocalPasswordEncoder
{
    /**
     * @var Hasher
     */
    private $encoder;

    /**
     * BcryptPasswordEncoder constructor.
     *
     * @param Hasher $encoder
     */
    public function __construct(Hasher $encoder)
    {
        $this->encoder = $encoder;
    }

    public function match(string $clearPassword, EncodedPassword $encodedPassword): bool
    {
        return $this->encoder->check($clearPassword, $encodedPassword->toString());
    }

    public function __invoke(LocalPassword $password): EncodedPassword
    {
        return EncodedPassword::fromString(
            $this->encoder->make($password->toString())
        );
    }
}