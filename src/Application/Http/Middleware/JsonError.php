<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Http\Middleware;

use Hewsda\Account\Response\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class JsonError
{
    public function handle(Request $request, \Closure $next)
    {
        try {
            return $next($request);

        } catch (\Throwable $e) {
            $contentType = trim($request->header('Content-Type'));

            if (0 === mb_strpos($contentType, 'application/json')) {

                // fixMe dev mode
                $data = [
                    'message' => $e->getMessage(),
                    'trace' => $e->getTraceAsString()
                ];

                /*
                $data = 'development' === getenv('PROOPH_ENV')
                    ? ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
                    : ['message' => 'Server Error'];
                */

                return new JsonResponse($data, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
    }
}