<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Http\Middleware;

use Illuminate\Http\Request;

class JsonPayload
{
    public function handle(Request $request, \Closure $next)
    {
        $contentType = $request->header('Content-Type');

        if (0 === strpos($contentType, 'application/json')) {
            $payload = json_decode((string)$request->getContent(), true);

            switch (json_last_error()) {
                case JSON_ERROR_DEPTH:
                    throw new \RuntimeException('Invalid JSON, maximum stack depth exceeded.', 400);
                case JSON_ERROR_UTF8:
                    throw new \RuntimeException('Malformed UTF-8 characters, possibly incorrectly encoded.', 400);
                case JSON_ERROR_SYNTAX:
                case JSON_ERROR_CTRL_CHAR:
                case JSON_ERROR_STATE_MISMATCH:
                    throw new \RuntimeException('Invalid JSON.', 400);
            }

            $request = $request->setJson($payload);
        }

        return $next($request);
    }
}