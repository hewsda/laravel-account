<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Http\Middleware;

use Hewsda\Account\Model\Account\LocalAccount\Query\GetAllAccounts;
use Illuminate\Http\Request;

class MessageAware
{
    public function handle(Request $request, \Closure $next)
    {
        if ($attribute = $this->findAttribute($request->route()->getName())) {
            $request->request->add($attribute);
        }

        return $next($request);
    }

    public function findAttribute(string $routeName = null): ?array
    {
        switch ($routeName) {
            case !$routeName:
                return null;

            case starts_with('query:', $routeName):
                return $this->findQueryAttribute($routeName);

            case starts_with('command:', $routeName):
                return $this->findCommandAttribute($routeName);

            default:
                return null;
        }
    }

    public function findCommandAttribute(string $routeName = null): ?array
    {
        switch ($routeName) {
            case 'command:accounts-list':
                return ['message_name' => GetAllAccounts::class];
        }


        return null;
    }

    public function findQueryAttribute(string $routeName = null): ?array
    {
        switch ($routeName) {
            case 'query:accounts-all':
                return ['message_name' => GetAllAccounts::class];
        }

        return null;
    }
}