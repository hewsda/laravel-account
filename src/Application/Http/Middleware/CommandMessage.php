<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Http\Middleware;

use Hewsda\Account\Application\ServiceBus\Metadata\MetadataGatherer;
use Hewsda\Account\Response\ResponseStrategy;
use Illuminate\Http\Request;
use Laraprooph\ServiceBus\Foundation\HasBus;
use Prooph\Common\Messaging\MessageFactory;
use Symfony\Component\HttpFoundation\Response;

class CommandMessage
{
    use HasBus;

    public const NAME_ATTRIBUTE = 'accountable_command_name';

    /**
     * @var MessageFactory
     */
    private $messageFactory;

    /**
     * @var ResponseStrategy
     */
    private $responseStrategy;

    /**
     * @var MetadataGatherer
     */
    private $metadataGatherer;

    /**
     * CommandMiddleware constructor.
     *
     * @param MessageFactory $messageFactory
     * @param ResponseStrategy $responseStrategy
     * @param MetadataGatherer $metadataGatherer
     */
    public function __construct(MessageFactory $messageFactory,
                                ResponseStrategy $responseStrategy,
                                MetadataGatherer $metadataGatherer
    )
    {
        $this->messageFactory = $messageFactory;
        $this->responseStrategy = $responseStrategy;
        $this->metadataGatherer = $metadataGatherer;
    }

    public function handle(Request $request): Response
    {
        $commandName = $request->get(self::NAME_ATTRIBUTE);

        if (!$commandName) {
            throw new \RuntimeException(
                sprintf('Command name attribute ("%s") was not found in request.', self::NAME_ATTRIBUTE),
                Response::HTTP_BAD_REQUEST
            );
        }

        try {
            $command = $this->messageFactory->createMessageFromArray($commandName, [
                'payload' => $request->json(),
                'metadata' => $this->metadataGatherer->getFromRequest($request),
            ]);

            $this->dispatchCommand($command);

            return $this->responseStrategy->withStatus(Response::HTTP_ACCEPTED);
        } catch (\Throwable $e) {
            throw new \RuntimeException(
                sprintf('An error occurred during dispatching of command "%s"', $commandName),
                Response::HTTP_INTERNAL_SERVER_ERROR,
                $e
            );
        }
    }
}