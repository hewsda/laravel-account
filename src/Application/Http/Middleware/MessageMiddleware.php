<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Http\Middleware;

use Assert\InvalidArgumentException;
use Hewsda\Account\Response\ResponseStrategy;
use Illuminate\Http\Request;
use Laraprooph\ServiceBus\Foundation\HasBus;
use Prooph\Common\Messaging\Message;
use Prooph\Common\Messaging\MessageFactory;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

class MessageMiddleware
{
    use HasBus;

    /**
     * @var ResponseStrategy
     */
    private $responseStrategy;

    /**
     * @var MessageFactory
     */
    private $messageFactory;

    /**
     * MessageMiddleware constructor.
     *
     * @param ResponseStrategy $responseStrategy
     * @param MessageFactory $messageFactory
     */
    public function __construct(ResponseStrategy $responseStrategy, MessageFactory $messageFactory)
    {
        $this->responseStrategy = $responseStrategy;
        $this->messageFactory = $messageFactory;
    }

    public function handle(Request $request): Response
    {
        $messageName = 'UNKNOWN';

        try {
            $payload = $this->getPayload($request);

            $message = $this->createMessageFromPayload($payload);
            $messageName = $message->messageName();

            return $this->dispatchMessage($message);
        } catch (InvalidArgumentException $exception) {
            throw new \RuntimeException(
                $exception->getMessage(),
                Response::HTTP_BAD_REQUEST,
                $exception
            );
        } catch (\Throwable $exception) {
            throw new \RuntimeException(
                sprintf('An error occurred during dispatching of message "%s"', $messageName),
                Response::HTTP_INTERNAL_SERVER_ERROR,
                $exception
            );
        }
    }

    private function dispatchMessage(Message $message): Response
    {
        switch ($message->messageType()) {
            case Message::TYPE_COMMAND:
                $this->dispatchCommand($message);

                return $this->responseStrategy->withStatus(Response::HTTP_ACCEPTED);

            case Message::TYPE_EVENT:
                $this->dispatchEvent($message);

                return $this->responseStrategy->withStatus(Response::HTTP_ACCEPTED);

            case Message::TYPE_QUERY:
                return $this->responseStrategy->fromPromise($this->dispatchQuery($message));

            default:
                throw new \RuntimeException(
                    sprintf(
                        'Invalid message type "%s" for message "%s".',
                        $message->messageType(),
                        $message->messageName()
                    ),
                    Response::HTTP_BAD_REQUEST
                );
        }
    }

    private function createMessageFromPayload(array $payload): Message
    {
        if (!isset($payload['uuid'])) {
            $payload['uuid'] = Uuid::uuid4();
        }

        if (!isset($payload['created_at'])) {
            $payload['created_at'] = new \DateTimeImmutable('now', new \DateTimeZone('UTC'));
        }

        if (!isset($payload['metadata'])) {
            $payload['metadata'] = [];
        }

        return $this->messageFactory->createMessageFromArray($payload['message_name'], $payload);
    }

    private function getPayload(Request $request): array
    {
        $payload = null;
        $messageName = 'UNKNOWN';

        $payload = $request->json();

        if (is_array($payload) && isset($payload['message_name'])) {
            $messageName = $payload['message_name'];
        }

        $messageName = $request->get('message_name', $messageName);

        $payload['message_name'] = $messageName;

        return $payload;
    }
}