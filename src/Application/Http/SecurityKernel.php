<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Http;

use Hewsda\Account\Application\Security\AccountActivation\AccountActivationAuthenticationFactory;
use Hewsda\Account\Application\Security\FormLogin\FormLoginAuthenticationFactory;
use Hewsda\Account\Application\Security\JwtApi\Firewall\Factory\JwtApiFormAuthenticationFactory;
use Hewsda\Account\Application\Security\JwtApi\Firewall\Factory\JwtApiPreAuthenticationFactory;
use Hewsda\Firewall\AuthenticationManager;
use Hewsda\FirewallJwt\JwtServiceProvider;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class SecurityKernel extends ServiceProvider
{
    // we could avoid that by setting this directly in firewall config
    // services => [ [form-login, FormLoginAuthFactory], [..] ]

    protected $firewalls = [
        'front_end' => [
            ['form-login', FormLoginAuthenticationFactory::class],
            ['account-activation', AccountActivationAuthenticationFactory::class],
        ],

        'api' => [
            ['jwt-api-login', JwtApiFormAuthenticationFactory::class],
            ['jwt-api-pre_auth', JwtApiPreAuthenticationFactory::class],
        ]
    ];

    public function boot(AuthenticationManager $manager): void
    {
        foreach ($this->firewalls as $name => $service) {
            foreach ($service as [$key, $factory]) {
                $manager->extend($name, $key, function (Application $app) use ($factory) {
                    return $app->make($factory);
                });
            }
        }

        $this->app->register(JwtServiceProvider::class);
    }
}