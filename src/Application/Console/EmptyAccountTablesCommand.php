<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Console;

use Illuminate\Console\Command;
use Illuminate\Database\Connection;

class EmptyAccountTablesCommand extends Command
{
    protected $signature = 'accountable:truncate-all-tables';

    protected $description = 'Empty all related account table';

    /**
     * @var Connection
     */
    private $connection;


    public function __construct(Connection $connection)
    {
        parent::__construct();

        $this->connection = $connection;
    }

    public function handle(): void
    {
        $this->connection->table('local_accounts')->truncate();
        $this->connection->table('account_role')->truncate();
        $this->connection->table('roles')->truncate();
        $this->connection->table('account_activation')->truncate();

        $this->info('All tables empty.');
    }
}