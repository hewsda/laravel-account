<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Console;

use Faker\Factory;
use Hewsda\Account\Application\Values\Account\LocalAccountId;
use Hewsda\Account\Model\Account\LocalAccount\Command\RegisterLocalAccount;
use Illuminate\Console\Command;
use Laraprooph\ServiceBus\Foundation\HasBus;

class CreateRandomAccountsCommand extends Command
{
    use HasBus;

    /**
     * @var string
     */
    protected $signature = 'accountable:create-accounts {num?}';

    /**
     * @var string
     */
    protected $description = 'Create random accounts';

    /**
     * @var Factory
     */
    private $factory;

    public function __construct(Factory $factory)
    {
        parent::__construct();

        $this->factory = $factory::create();
    }

    public function handle(): void
    {
        $num = $this->argument('num') ?? 50;

        $this->info('Going to create ' . $num . ' accounts.');

        $this->output->progressStart($num);

        for ($i = 0; $i < $num; $i++) {
            $this->dispatchCommand(RegisterLocalAccount::withData(
                $accountId = LocalAccountId::nextIdentity()->toString(),
                $this->factory->name,
                $this->factory->email,
                'password', 'password'
            ));

            $this->output->progressAdvance();
        }

        $this->output->progressFinish();

        $this->info('All accounts successfully registered.');
    }
}