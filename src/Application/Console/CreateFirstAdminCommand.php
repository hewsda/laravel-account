<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Console;

use Hewsda\Account\Application\ServiceBus\Backend\BackendCommandBus;
use Hewsda\Account\Application\Values\Account\LocalAccountId;
use Hewsda\Account\Model\Account\LocalAccount\Command\CreateActivatedAccount;
use Hewsda\Account\Model\Account\LocalAccount\Command\GiveRole;
use Hewsda\Account\Model\Account\LocalAccount\Handler\CreateLocalAccount;
use Illuminate\Console\Command;
use Prooph\ServiceBus\Exception\CommandDispatchException;

class CreateFirstAdminCommand extends Command
{

    /**
     * @var string
     */
    protected $signature = 'accountable:create-first-admin';

    /**
     * @var string
     */
    protected $description = 'create the first admin account';

    /**
     * @var BackendCommandBus
     */
    private $commandBus;

    public function __construct(BackendCommandBus $commandBus)
    {
        parent::__construct();

        $this->commandBus = $commandBus;
    }

    public function handle(): void
    {
        $name = $this->ask('What is your name?');
        $email = $this->ask('What is your email?');
        $password = $this->secret('What is your password?');

        try {
            $this->commandBus->dispatch(new CreateLocalAccount(
                [
                    'account_id' => $accountId = LocalAccountId::nextIdentity()->toString(),
                    'email' => $email,
                    'name' => $name,
                    'password' => $password,
                    'password_confirmation' => $password
                ]
            ));

            $this->commandBus->dispatch(GiveRole::forAccount($accountId, $accountId, 'ROLE_ADMIN'));

            $this->commandBus->dispatch(CreateActivatedAccount::forAccount($accountId));

            $this->info(sprintf('Account successfully created with id %s', $accountId));

        } catch (CommandDispatchException $exception) {
            $this->error($exception->getPrevious()->getMessage());
        }
    }
}