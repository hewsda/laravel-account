<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Console;

use Hewsda\Account\Application\ServiceBus\Backend\BackendCommandBus;
use Hewsda\Account\Model\Role\Command\CreateRole;
use Illuminate\Console\Command;
use Prooph\ServiceBus\Exception\CommandDispatchException;

class CreateRoleCommand extends Command
{

    /**
     * @var string
     */
    protected $signature = 'accountable:create-role {role*}';

    /**
     * @var string
     */
    protected $description = 'Create role with name and description';

    /**
     * @var BackendCommandBus
     */
    private $commandBus;

    /**
     * CreateRoleCommand constructor.
     *
     * @param BackendCommandBus $commandBus
     */
    public function __construct(BackendCommandBus $commandBus)
    {
        parent::__construct();

        $this->commandBus = $commandBus;
    }

    public function handle(): void
    {
        try {
            $this->commandBus->dispatch(CreateRole::withData(
                $role = $this->arguments()['role'][0],
                $this->arguments()['role'][1]
            ));

            $this->info(sprintf('Role %s created', $role));
        } catch (CommandDispatchException $exception) {
            $this->error($exception->getPrevious()->getMessage());
        }
    }
}