<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Console;

use Hewsda\Account\Application\ServiceBus\Backend\BackendCommandBus;
use Hewsda\Account\Model\Role\Command\CreateRole;
use Hewsda\Account\Model\Role\Repository\RoleList;
use Illuminate\Console\Command;
use Laraprooph\ServiceBus\Foundation\HasBus;
use Prooph\ServiceBus\Exception\CommandDispatchException;

class SeedRoleTableCommand extends Command
{
    use HasBus;

    /**
     * @var string
     */
    protected $signature = 'accountable:seed-roles';

    /**
     * @var string
     */
    protected $description = 'Seed role table with basic roles.';

    /**
     * @var RoleList
     */
    private $roleList;
    /**
     * @var BackendCommandBus
     */
    private $commandBus;

    /**
     * SeedRoleTableCommand constructor.
     *
     * @param RoleList $roleList
     * @param BackendCommandBus $commandBus
     */
    public function __construct(RoleList $roleList, BackendCommandBus $commandBus)
    {
        parent::__construct();

        $this->roleList = $roleList;
        $this->commandBus = $commandBus;
    }

    public function handle(): void
    {
        // clear cache if cache decorator.

        try {
            foreach ($this->getRoles() as [$role, $description]) {
                $this->commandBus->dispatch(CreateRole::withData($role, $description));
            }

            $this->info('All roles seeded.');
        } catch (CommandDispatchException $exception) {
            $this->error($exception->getPrevious()->getMessage());
        }
    }

    protected function getRoles(): array
    {
        return [
            ['ROLE_USER', 'Basic account role'],
            ['ROLE_MODERATOR', 'Extra permission'],
            ['ROLE_API_ADMIN', 'Access api reserved'],
            ['ROLE_ADMIN', 'God role']
        ];
    }
}