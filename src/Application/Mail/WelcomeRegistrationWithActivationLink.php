<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Mail;

use Hewsda\Account\Application\Security\AccountActivation\ActivationCode;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WelcomeRegistrationWithActivationLink extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var ActivationCode
     */
    private $code;
    /**
     * @var string
     */
    private $name;

    /**
     * WelcomeRegistrationWithActivationLink constructor.
     *
     * @param string $name
     * @param ActivationCode $code
     */
    public function __construct(string $name, ActivationCode $code)
    {
        $this->code = $code;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.registration_welcome_with_activation_link',
            ['name' => $this->name, 'code' => $this->code]);
    }
}