<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Mail;

use Hewsda\Account\Model\Account\PasswordReset\PasswordResetCode;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordResetLink extends Mailable
{

    use Queueable, SerializesModels;


    /**
     * @var PasswordResetCode
     */
    private $code;

    /**
     * PasswordResetLink constructor.
     *
     * @param PasswordResetCode $code
     */
    public function __construct(PasswordResetCode $code)
    {
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.password_reset_link', ['code' => $this->code->toString()]);
    }
}