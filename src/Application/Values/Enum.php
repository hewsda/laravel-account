<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Values;

use Hewsda\Security\Foundation\Contracts\Value as SecurityValue;
use MabeEnum\EnumSerializableTrait;

abstract class Enum extends \MabeEnum\Enum implements \Serializable, Value
{
    use EnumSerializableTrait;

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $this->is($aValue);
    }

    public function toString(): string
    {
        return $this->getName();
    }
}