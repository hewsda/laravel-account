<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Values;

use Hewsda\Security\Foundation\Contracts\Value as SecurityValue;

interface Value extends SecurityValue
{
}