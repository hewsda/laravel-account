<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Values\Account;

use Hewsda\Security\Foundation\Contracts\Value;

class EncodedPassword extends Password
{
    public static function fromString($encodedPassword): EncodedPassword
    {
        return new self($encodedPassword);
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof EncodedPassword && $this->password === $aValue->toString();
    }
}