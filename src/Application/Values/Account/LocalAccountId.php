<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Values\Account;

use Hewsda\Account\Application\Values\Identification;
use Hewsda\Security\Foundation\Value\UserId;
use Ramsey\Uuid\Uuid;

class LocalAccountId extends UserId implements Identification
{
    public static function fromString($userId): UserId
    {
        return new self(Uuid::fromString($userId));
    }

    public static function nextIdentity(): UserId
    {
        return new self(Uuid::uuid4());
    }
}