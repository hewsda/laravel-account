<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Values\Account;

use Hewsda\Account\Application\Exceptions\Assertion;
use Hewsda\Security\Foundation\Contracts\Value;

class LocalPassword extends Password
{

    public static function fromStringWithConfirmation($password, $confirmedPassword): LocalPassword
    {
        Assertion::same($password, $confirmedPassword, 'Password and his confirmation does not match.');

        return new self($password);
    }

    public static function fromString($password): LocalPassword
    {
        return new self($password);
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof LocalPassword && $this->password === $aValue->toString();
    }
}