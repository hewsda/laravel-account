<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Values\Account;

use Hewsda\Account\Application\Exceptions\Assertion;
use Hewsda\Account\Application\Values\Identification;
use Hewsda\Security\Foundation\Value\EmailAddress;

class AccountEmail extends EmailAddress implements Identification
{
    public static function fromString($email): EmailAddress
    {
        Assertion::string($email, self::$message);
        Assertion::email($email, self::$message);

        return new self($email);
    }
}