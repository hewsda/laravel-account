<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Values\Account;

use Hewsda\Account\Application\Exceptions\Assertion;
use Hewsda\Account\Application\Values\Value;

abstract class Password implements Value
{
    const MIN_LENGTH = 8;
    const MAX_LENGTH = 255;

    /**
     * @var string
     */
    protected $password;

    /**
     * Password constructor.
     *
     * @param string $password
     */
    protected function __construct($password)
    {
        $this->validatePassword($password);

        $this->password = $password;
    }

    protected function validatePassword($password): void
    {
        Assertion::string($password, 'Password is invalid.');
        Assertion::betweenLength($password, self::MIN_LENGTH, self::MAX_LENGTH,
            'Password must be between'. self::MIN_LENGTH . ' and ' . self::MAX_LENGTH . ' characters');
    }

    public function toString(): string
    {
        return $this->password;
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}