<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Values;

use Hewsda\Security\Foundation\Contracts\Core\User\Identifier;

interface Identification extends Identifier
{

}