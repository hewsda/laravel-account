<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Values\Role;

use Hewsda\Account\Application\Exceptions\Assertion;
use Hewsda\Security\Core\Role\RoleValue;

class FormRoleCreation
{
    /**
     * @var RoleValue
     */
    private $role;

    /**
     * @var string
     */
    private $description;

    /**
     * RoleCreator constructor.
     *
     * @param RoleValue $roleValue
     * @param string $description
     */
    private function __construct(RoleValue $roleValue, string $description)
    {
        $this->role = $roleValue;
        $this->description = $description;
    }

    public static function withData(string $roleName, string $description): FormRoleCreation
    {
        Assertion::notEmpty($description, 'You must provide a description for your role.');

        return new self(RoleValue::fromString($roleName), $description);
    }

    public function slug(): string
    {
        return str_slug($this->role->getRole());
    }

    public function role(): string
    {
        return $this->role->getRole();
    }

    public function description(): string
    {
        return $this->description;
    }
}