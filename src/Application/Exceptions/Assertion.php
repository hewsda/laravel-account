<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Exceptions;

use Assert\Assertion as BaseAssertion;

class Assertion extends BaseAssertion
{
    protected static $exceptionClass = AccountValidationException::class;
}