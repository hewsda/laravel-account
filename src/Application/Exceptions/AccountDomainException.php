<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Exceptions;

class AccountDomainException extends \RuntimeException
{
}