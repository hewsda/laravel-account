<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Providers;

use Hewsda\Account\Infrastructure\Repository\Account\Local\LocalAccountRepository;
use Hewsda\Account\Infrastructure\Services\BcryptPasswordEncoder;
use Hewsda\Account\Infrastructure\Services\UniqueLocalAccountEmailAddress;
use Hewsda\Account\Model\Account\LocalAccount\Repository\LocalAccountRepositoryContract;
use Hewsda\Account\Model\Account\LocalAccount\Services\CheckUniqueLocalEmailAddress;
use Hewsda\Account\Model\Account\LocalAccount\Services\LocalPasswordEncoder;
use Illuminate\Support\ServiceProvider;

class AccountServiceProvider extends ServiceProvider
{
    protected $services = [
        LocalPasswordEncoder::class => BcryptPasswordEncoder::class,
        CheckUniqueLocalEmailAddress::class => UniqueLocalAccountEmailAddress::class
    ];

    public function boot(): void
    {
        foreach ($this->services as $abstract => $concrete) {
            $this->app->bindIf($abstract, $concrete);
        }
    }

    public function register(): void
    {
        $this->registerLocalAccountProvider();
    }

    private function registerLocalAccountProvider(): void
    {
        $this->app->bind(LocalAccountRepositoryContract::class, LocalAccountRepository::class);
    }
}