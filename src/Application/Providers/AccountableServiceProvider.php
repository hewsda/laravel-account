<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Providers;

use Hewsda\Account\Application\Http\SecurityKernel;
use Hewsda\Firewall\FirewallServiceProvider;
use Illuminate\Support\AggregateServiceProvider;

class AccountableServiceProvider extends AggregateServiceProvider
{
    protected $providers = [

        // add some voters
        // Need to rethink workflow secure payload/ mark internal
        AuthorizationServiceProvider::class,

        FirewallServiceProvider::class,

        // Repo
        RoleRepositoryServiceProvider::class,
        AccountServiceProvider::class,

        // auth factories
        SecurityKernel::class,

        // Service bus registry
        ServiceBusServiceProvider::class,

        // Console
        CommandConsoleServiceProvider::class
    ];
}