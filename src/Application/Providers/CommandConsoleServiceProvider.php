<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Providers;

use Hewsda\Account\Application\Console\CreateFirstAdminCommand;
use Hewsda\Account\Application\Console\CreateRandomAccountsCommand;
use Hewsda\Account\Application\Console\CreateRoleCommand;
use Hewsda\Account\Application\Console\EmptyAccountTablesCommand;
use Hewsda\Account\Application\Console\SeedRoleTableCommand;
use Illuminate\Support\ServiceProvider;

class CommandConsoleServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->commands($this->registerCommands());
    }

    private function registerCommands(): array
    {
        return [
            CreateRandomAccountsCommand::class,
            EmptyAccountTablesCommand::class,
            SeedRoleTableCommand::class,
            CreateRoleCommand::class,
            CreateFirstAdminCommand::class
        ];
    }
}