<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Providers;

use Hewsda\Account\Application\ServiceBus\Async\AsyncAccountCommandBus;
use Hewsda\Account\Application\ServiceBus\Backend\BackendCommandBus;
use Hewsda\Account\Application\ServiceBus\Metadata\MetadataGatherer;
use Hewsda\Account\Application\ServiceBus\Metadata\NoopMetadataGatherer;
use Hewsda\Account\Response\JsonResponse;
use Hewsda\Account\Response\ResponseStrategy;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Prooph\Common\Messaging\FQCNMessageFactory;
use Prooph\Common\Messaging\MessageConverter;
use Prooph\Common\Messaging\MessageFactory;
use Prooph\Common\Messaging\NoOpMessageConverter;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\EventBus;

class ServiceBusServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    /**
     * @var array
     */
    protected $messageBus = [
        'command' => [
            ['command_bus', CommandBus::class],
            ['async_account_command_bus', AsyncAccountCommandBus::class],
            ['backend_command_bus', BackendCommandBus::class]
        ],
        'event' => [
            ['event_bus', EventBus::class]
        ]
    ];

    public function boot(): void
    {
        foreach ($this->messageBus as $busType => $types) {
            foreach ($types as [$serviceId, $alias]) {
                $this->app->singleton($serviceId, function (Application $app) use ($serviceId, $busType) {
                    return $app['bus_manager.' . $busType]->{$busType}($serviceId);
                });

                $this->app->alias($serviceId, $alias);
            }
        }
    }

    public function register(): void
    {
        $this->app->bind(MessageConverter::class, NoOpMessageConverter::class);

        $this->app->bind(MessageFactory::class, FQCNMessageFactory::class);

        $this->app->bind(ResponseStrategy::class, JsonResponse::class);

        $this->app->bind(MetadataGatherer::class, NoopMetadataGatherer::class);
    }

    public function provides(): array
    {
        return [
            MessageConverter::class,
            MessageFactory::class,
            ResponseStrategy::class,
            MetadataGatherer::class
        ];
    }
}