<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Providers;

use Hewsda\Account\Infrastructure\Repository\Role\RoleCacheCollection;
use Hewsda\Account\Model\Role\Repository\RoleList;
use Illuminate\Support\ServiceProvider;

class RoleRepositoryServiceProvider extends ServiceProvider
{
    protected $defer = true;

    public function register(): void
    {
        $this->app->bind(RoleList::class, RoleCacheCollection::class);
    }

    public function provides(): array
    {
        return [RoleList::class];
    }
}