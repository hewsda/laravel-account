<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Providers;

use Hewsda\Account\Application\ServiceBus\Authorization\AuthorizationService;
use Hewsda\Account\Application\ServiceBus\Authorization\FQCNMessageNameVoter;
use Hewsda\Firewall\Foundation\Providers\VoterServiceProvider;
use Illuminate\Support\ServiceProvider;
use Prooph\ServiceBus\Plugin\Guard\AuthorizationService as ProophAuthorization;

class AuthorizationServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $tag = VoterServiceProvider::TAG_VOTERS;

        $this->app->tag(FQCNMessageNameVoter::class, $tag);

        $this->app->bind(ProophAuthorization::class, AuthorizationService::class);
    }
}