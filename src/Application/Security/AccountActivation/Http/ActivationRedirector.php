<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\AccountActivation\Http;

use Hewsda\Security\Core\Exception\AuthenticationException;
use Hewsda\Security\Foundation\Contracts\Core\Authentication\Token\Tokenable;
use Hewsda\Security\Foundation\Contracts\Http\Authentication\AuthenticationFailure;
use Hewsda\Security\Foundation\Contracts\Http\Authentication\AuthenticationSuccess;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ActivationRedirector implements AuthenticationFailure, AuthenticationSuccess
{
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * @var string
     */
    private $route;

    /**
     * UserActivationRedirector constructor.
     *
     * @param ResponseFactory $responseFactory
     * @param string $route
     */
    public function __construct(ResponseFactory $responseFactory, string $route)
    {
        $this->responseFactory = $responseFactory;
        $this->route = $route;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
    {
        return $this->responseFactory
            ->redirectToRoute($this->route)
            ->with('message', 'Activation code not found or has been expired.');
    }

    public function onAuthenticationSuccess(Request $request, Tokenable $token): Response
    {
        $message = 'Account activation successful, you can now login %s';

        return $this->responseFactory
            ->redirectToRoute($this->route)
            ->with('message', sprintf($message, $token->user()->getEmail()->toString()));
    }
}