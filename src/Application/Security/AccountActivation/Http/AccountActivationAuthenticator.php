<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\AccountActivation\Http;

use Hewsda\Account\Application\Security\AccountActivation\AccountActivationToken;
use Hewsda\Account\Application\Security\AccountActivation\ActivationCode;
use Hewsda\Account\Model\Account\LocalAccount\Command\ActivateAccount;
use Hewsda\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Hewsda\Security\Core\Exception\AuthenticationException;
use Hewsda\Security\Core\Exception\BadCredentials;
use Hewsda\Security\Foundation\Contracts\Core\Authentication\SimplePreAuthenticator;
use Hewsda\Security\Foundation\Contracts\Core\Authentication\Token\Tokenable;
use Hewsda\Security\Foundation\Contracts\Core\User\UserProvider;
use Hewsda\Security\Foundation\Contracts\Http\Authentication\AuthenticationFailure;
use Hewsda\Security\Foundation\Contracts\Http\Authentication\AuthenticationRequest;
use Hewsda\Security\Foundation\Contracts\Http\Authentication\AuthenticationSuccess;
use Hewsda\Security\Foundation\Value\ProviderKey;
use Illuminate\Http\Request;
use Laraprooph\ServiceBus\Foundation\HasBus;
use Symfony\Component\HttpFoundation\Response;

class AccountActivationAuthenticator implements SimplePreAuthenticator, AuthenticationFailure, AuthenticationSuccess
{
    use HasBus;

    /**
     * @var AuthenticationRequest
     */
    private $authenticationRequest;

    /**
     * @var ActivationRedirector
     */
    private $redirector;

    /**
     * AccountActivationAuthenticator constructor.
     *
     * @param AuthenticationRequest $authenticationRequest
     * @param ActivationRedirector $redirector
     */
    public function __construct(AuthenticationRequest $authenticationRequest, ActivationRedirector $redirector)
    {
        $this->authenticationRequest = $authenticationRequest;
        $this->redirector = $redirector;
    }

    public function authenticateToken(Tokenable $authenticatedToken, UserProvider $userProvider, ProviderKey $providerKey): Tokenable
    {
        $activationKey = $authenticatedToken->credentials();

        $user = $userProvider->requireByIdentifier($activationKey);

        $authenticatedToken = new PreAuthenticatedToken($user, $activationKey, $providerKey, $user->getRoles());

        $this->dispatchCommand(ActivateAccount::forAccount(
                $user->getId()->toString(),
                $activationKey->code())
        );

        return $authenticatedToken;
    }

    public function createToken(Request $request, ProviderKey $providerKey): Tokenable
    {
        $credentials = $this->requireCredentialsFromRequest($request);

        return new AccountActivationToken('.anon', $credentials, $providerKey);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
    {
        return $this->redirector->onAuthenticationFailure($request, $exception);
    }

    public function onAuthenticationSuccess(Request $request, Tokenable $token): Response
    {
        return $this->redirector->onAuthenticationSuccess($request, $token);
    }

    public function supportsToken(Tokenable $token, ProviderKey $providerKey): bool
    {
        return $token instanceof AccountActivationToken &&
            $token->providerKey()->sameValueAs($providerKey);
    }

    private function requireCredentialsFromRequest(Request $request): ActivationCode
    {
        $token = $this->authenticationRequest->extract($request);

        if (!$token) {
            throw new BadCredentials('Invalid credentials.');
        }

        return $token;
    }
}