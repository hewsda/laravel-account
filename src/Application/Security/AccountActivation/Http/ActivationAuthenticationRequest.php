<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\AccountActivation\Http;

use Hewsda\Account\Application\Security\AccountActivation\ActivationCode;
use Hewsda\Security\Foundation\Contracts\Http\Authentication\AuthenticationRequest;
use Illuminate\Http\Request as IlluminateRequest;
use Symfony\Component\HttpFoundation\Request;

class ActivationAuthenticationRequest implements AuthenticationRequest
{

    public function extract(IlluminateRequest $request): ?ActivationCode
    {
        if ($this->matches($request)) {
            return ActivationCode::fromString(
                $request->route('activation_code')
            );
        }

        return null;
    }

    public function matches(Request $request): bool
    {
        return 'front.account.activation' === $request->route()->getName();
    }
}