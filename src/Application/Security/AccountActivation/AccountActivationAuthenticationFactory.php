<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\AccountActivation;

use Hewsda\Account\Application\Security\AccountActivation\Http\AccountActivationAuthenticator;
use Hewsda\Account\Application\Security\AccountActivation\Http\ActivationAuthenticationRequest;
use Hewsda\Account\Application\Security\AccountActivation\Http\ActivationRedirector;
use Hewsda\Firewall\Factory\Payload\PayloadFactory;
use Hewsda\Firewall\Factory\Payload\PayloadService;
use Hewsda\Firewall\Factory\Services\AuthenticationServiceFactory;
use Hewsda\Security\Core\Authentication\Provider\SimpleProxyAuthenticationProvider;
use Hewsda\Security\Foundation\Value\ProviderKey;
use Hewsda\Security\Http\Firewall\SimplePreAuthenticationListener;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;

class AccountActivationAuthenticationFactory extends AuthenticationServiceFactory
{
    public function register(PayloadService $payload): PayloadFactory
    {
        $authenticatorId = $this->registerAuthenticator($payload->firewallKey);

        return (new PayloadFactory())
            ->setListener($this->registerListener($payload, $authenticatorId))
            ->setProvider($this->registerProvider($payload, $authenticatorId));
    }

    protected function registerListener(PayloadService $payload, string $authenticatorId): string
    {
        $listenerId = 'firewall.' . $this->position() . '_authentication_listener.' . $this->key() . '.' . $payload->firewallId();

        $this->container->bind($listenerId, function (Application $app) use ($payload, $authenticatorId) {
            return new SimplePreAuthenticationListener(
                $payload->firewallKey,
                $app->make($authenticatorId),
                $this->matcher(),
                true
            );
        });

        return $listenerId;
    }

    protected function registerAuthenticator(ProviderKey $key): string
    {
        $id = 'firewall.' . $this->key() . '.' . $this->position() . '.authenticator.' . $key->getKey();

        $this->container->bind($id, function () {
            return new AccountActivationAuthenticator(
                $this->matcher(),
                $this->redirector()
            );
        });

        return $id;
    }

    protected function registerProvider(PayloadService $payload, string $authenticatorId): string
    {
        $providerId = 'firewall.' . $this->position() . '_authentication_provider.' . $this->key() . '.' . $payload->firewallId();

        $this->container->bind($providerId, function (Application $app) use ($payload, $authenticatorId) {
            return new SimpleProxyAuthenticationProvider(
                $app->make($authenticatorId),
                $app->make($payload->userProviderId),
                $payload->firewallKey
            );
        });

        return $providerId;
    }

    protected function redirector(): Http\ActivationRedirector
    {
        return new ActivationRedirector(
            $this->container->make(ResponseFactory::class), 'front.login');
    }

    public function position(): string
    {
        return 'pre_auth';
    }

    public function key(): string
    {
        return 'account-activation';
    }

    public function isRequired(): bool
    {
        return false;
    }

    public function matcher(): ?RequestMatcherInterface
    {
        return new ActivationAuthenticationRequest();
    }
}