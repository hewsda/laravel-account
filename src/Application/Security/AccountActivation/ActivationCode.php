<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\AccountActivation;

use Hewsda\Account\Application\Values\Identification;
use Hewsda\Security\Foundation\Assertion\Assertion;
use Hewsda\Security\Foundation\Contracts\Value;

class ActivationCode implements Value, Identification
{
    const TOKEN_LENGTH = 60;

    /**
     * @var string
     */
    private $code;

    /**
     * ActivationToken constructor.
     *
     * @param string $code
     */
    private function __construct(string $code)
    {
        $this->code = $code;
    }

    public static function nextToken(): ActivationCode
    {
        return new self(self::generateToken());
    }

    public static function fromString($token): ActivationCode
    {
        $message = 'Activation code is not valid.';

        Assertion::string($token, $message);

        Assertion::length($token, self::TOKEN_LENGTH, $message);

        return new self($token);
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof $this &&
            $this->code === $aValue->code();
    }

    public function code(): string
    {
        return $this->code;
    }

    private static function generateToken(): string
    {
        return str_random(self::TOKEN_LENGTH);
    }

    public function __toString(): string
    {
        return $this->code();
    }
}