<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\AccountActivation;

use Hewsda\Security\Core\Authentication\Token\PreAuthenticatedToken;

class AccountActivationToken extends PreAuthenticatedToken
{
}