<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\JwtApi\Firewall;

use Hewsda\FirewallJwt\Contracts\ClaimFactory;
use Hewsda\Security\Foundation\Contracts\Core\User\LocalUser;
use Illuminate\Http\Request;
use Lcobucci\JWT\Builder;

class ApiClaims implements ClaimFactory
{

    public function __invoke(Builder $builder): callable
    {
        return function (LocalUser $user, Request $request) use ($builder) {
            $builder->setIssuedAt(time());
            $builder->setExpiration(time() + 1000);
            $builder->set('email', (string)$user->getEmail()->toString());
            $builder->setIssuer($request->root());
            $builder->setAudience($request->root() . '/api');
            $builder->setId($user->getId()->toString(), true);

            return $builder;
        };
    }
}