<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\JwtApi\Firewall;

use Hewsda\FirewallJwt\Contracts\ClaimValidation;
use Hewsda\FirewallJwt\Exception\FirewallJwtAuthenticationException;
use Hewsda\Security\Foundation\Contracts\Core\User\LocalUser;
use Illuminate\Http\Request;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\ValidationData;

class JwtApiValidation implements ClaimValidation
{

    public function __invoke(Token $token, LocalUser $account, Request $request): ValidationData
    {
        $message = 'Jwt token validation failed.';

        if (!$token->hasClaim('email')) {
            throw new FirewallJwtAuthenticationException($message);
        }

        if( $token->getClaim('email') !== $account->getEmail()->toString()){
            throw new FirewallJwtAuthenticationException($message);
        }

        if ($token->getClaim('iat') > $token->getClaim('exp')) {
            throw new FirewallJwtAuthenticationException('Jwt token has been expired.');
        }

        return new ValidationData();
    }
}