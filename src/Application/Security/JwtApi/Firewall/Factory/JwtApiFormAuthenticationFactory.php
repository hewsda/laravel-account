<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\JwtApi\Firewall\Factory;

use Hewsda\Account\Application\Security\JwtApi\Http\Authentication\JwtApiFormSuccess;
use Hewsda\Account\Application\Security\JwtApi\Http\Firewall\JwtFormListener;
use Hewsda\Account\Application\Security\JwtApi\Http\Request\JwtApiLoginAuthenticationRequest;
use Hewsda\Firewall\Factory\Payload\PayloadService;
use Hewsda\Security\Core\Authentication\Provider\GenericFormAuthenticationProvider;
use Hewsda\Security\Core\User\UserChecker;
use Hewsda\Security\Foundation\Contracts\Http\Authentication\AuthenticationSuccess;
use Illuminate\Contracts\Foundation\Application;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;

class JwtApiFormAuthenticationFactory extends JwtApiAuthentication
{

    public function position(): string
    {
        return 'form';
    }

    public function key(): string
    {
        return 'jwt-api-login';
    }

    public function matcher(): ?RequestMatcherInterface
    {
        return new JwtApiLoginAuthenticationRequest();
    }

    public function isRequired(): bool
    {
        return true;
    }

    public function registerListener(PayloadService $payload): string
    {
        $id = 'firewall.' . $this->position() . '.' . $this->key() . '_authentication_listener.' . $payload->firewallId();

        // Pass a success handler for public api
        // in this case we only grant some type of account
        $this->container->bind($id, function () use ($payload) {
            return new JwtFormListener(
                $payload->firewallKey,
                $this->failureHandler($payload),
                $this->matcher()
            );
        });

        return $id;
    }

    public function registerProvider(PayloadService $payload): string
    {
        $id = 'firewall.' . $this->position() . '.' . $this->key() . '_authentication_provider.' . $payload->firewallId();

        $this->container->bindIf($id, function (Application $app) use ($payload) {
            return new GenericFormAuthenticationProvider(
                $payload->firewallKey,
                $app->make($payload->userProviderId),
                new UserChecker(),
                $app->make('hash'),
                true
            );
        });

        return $id;

    }

    public function successHandler(): AuthenticationSuccess
    {
        return new JwtApiFormSuccess(
            $this->container->make($this->serviceManagerId())
        );
    }
}