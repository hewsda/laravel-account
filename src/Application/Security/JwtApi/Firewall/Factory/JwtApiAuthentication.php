<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\JwtApi\Firewall\Factory;

use Hewsda\Firewall\Factory\Payload\PayloadFactory;
use Hewsda\Firewall\Factory\Payload\PayloadService;
use Hewsda\Firewall\Foundation\Contracts\Factory\AuthenticationServiceFactory;
use Hewsda\FirewallJwt\JwtServiceManager;
use Hewsda\Security\Foundation\Contracts\Http\Authentication\AuthenticationFailure;
use Hewsda\Security\Http\Authentication\JsonFailureHandler;
use Hewsda\Security\Http\Entrypoint\JsonEntrypoint;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Foundation\Application;

abstract class JwtApiAuthentication implements AuthenticationServiceFactory
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @var string
     */
    private $serviceManagerId;

    /**
     * JwtApiAuthentication constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function register(PayloadService $payload): PayloadFactory
    {
        $this->serviceManagerId = $this->registerServiceManager($payload);

        return (new PayloadFactory())
            ->setListener($this->registerListener($payload))
            ->setProvider($this->registerProvider($payload))
            ->setEntrypoint($this->registerEntrypoint($payload));
    }

    abstract public function registerListener(PayloadService $payload): string;

    abstract public function registerProvider(PayloadService $payload): string;

    private function registerServiceManager(PayloadService $payload): string
    {
        $id = 'firewall.jwt_service_manager' . $payload->firewallId();

        $this->container->bind($id, function (Application $app) use ($payload) {
            return $app->make(JwtServiceManager::class)->create($payload->firewallId());
        });

        return $id;
    }

    public function registerEntrypoint(PayloadService $payload): string
    {
        $entryPointId = 'security.authentication.jwt_api_entry_point.' . $payload->firewallId();

        $this->container->bindIf($entryPointId, JsonEntryPoint::class);

        return $entryPointId;
    }

    public function failureHandler(PayloadService $payload): AuthenticationFailure
    {
        // need api response
        return new JsonFailureHandler();
    }

    public function userProviderKey(): ?string
    {
        return null;
    }

    public function serviceManagerId(): string
    {
        return $this->serviceManagerId;
    }
}