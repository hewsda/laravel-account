<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\JwtApi\Firewall\Factory;

use Hewsda\Account\Application\Security\JwtApi\Core\Authentication\JwtApiAuthenticationProvider;
use Hewsda\Account\Application\Security\JwtApi\Http\Firewall\JwtApiPreAuthenticationListener;
use Hewsda\Account\Application\Security\JwtApi\Http\Request\JwtApiPreAuthenticationRequest;
use Hewsda\Firewall\Factory\Payload\PayloadService;
use Hewsda\Security\Core\User\UserChecker;
use Hewsda\Security\Foundation\Contracts\Core\Authentication\TrustResolver;
use Illuminate\Contracts\Foundation\Application;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;

class JwtApiPreAuthenticationFactory extends JwtApiAuthentication
{

    public function position(): string
    {
        return 'pre_auth';
    }

    public function key(): string
    {
        return 'jwt-api-pre_auth';
    }

    public function matcher(): ?RequestMatcherInterface
    {
        return new JwtApiPreAuthenticationRequest();
    }

    public function isRequired(): bool
    {
        return true;
    }

    public function registerListener(PayloadService $payload): string
    {
        $id = 'firewall.' . $this->position() . '.' . $this->key() . '_authentication_listener.' . $payload->firewallId();

        $this->container->bind($id, function (Application $app) use ($payload) {
            return new JwtApiPreAuthenticationListener(
                $payload->firewallKey,
                $this->matcher(),
                $this->failureHandler($payload),
                $app->make(TrustResolver::class)
            );
        });

        return $id;
    }

    public function registerProvider(PayloadService $payload): string
    {
        $id = 'firewall.' . $this->position() . '.' . $this->key() . '_authentication_provider.' . $payload->firewallId();

        $this->container->bind($id, function (Application $app) use ($payload) {
            return new JwtApiAuthenticationProvider(
                $payload->firewallKey,
                new UserChecker(),
                $app->make($this->serviceManagerId()),
                $app->make($payload->userProviderId)
            );
        });

        return $id;
    }
}