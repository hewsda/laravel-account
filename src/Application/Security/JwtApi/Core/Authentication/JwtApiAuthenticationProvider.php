<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\JwtApi\Core\Authentication;

use Hewsda\FirewallJwt\Exception\FirewallJwtException;
use Hewsda\FirewallJwt\JwtGuard;
use Hewsda\Security\Core\Authentication\Token\UserToken;
use Hewsda\Security\Core\Exception\AuthenticationException;
use Hewsda\Security\Foundation\Contracts\Core\Authentication\AuthenticationProvider;
use Hewsda\Security\Foundation\Contracts\Core\Authentication\Token\Tokenable;
use Hewsda\Security\Foundation\Contracts\Core\User\LocalUser;
use Hewsda\Security\Foundation\Contracts\Core\User\UserChecker;
use Hewsda\Security\Foundation\Contracts\Core\User\UserProvider;
use Hewsda\Security\Foundation\Value\EmailAddress;
use Hewsda\Security\Foundation\Value\ProviderKey;
use Lcobucci\JWT\Token;

class JwtApiAuthenticationProvider implements AuthenticationProvider
{
    /**
     * @var ProviderKey
     */
    private $providerKey;

    /**
     * @var UserChecker
     */
    private $userChecker;

    /**
     * @var JwtGuard
     */
    private $jwtGuard;

    /**
     * @var UserProvider
     */
    private $userProvider;

    /**
     * JwtApiAuthenticationProvider constructor.
     *
     * @param ProviderKey $providerKey
     * @param UserChecker $userChecker
     * @param JwtGuard $jwtGuard
     * @param UserProvider $userProvider
     */
    public function __construct(ProviderKey $providerKey, UserChecker $userChecker, JwtGuard $jwtGuard, UserProvider $userProvider)
    {
        $this->providerKey = $providerKey;
        $this->userChecker = $userChecker;
        $this->jwtGuard = $jwtGuard;
        $this->userProvider = $userProvider;
    }

    public function __invoke(Tokenable $token): Tokenable
    {
        try {
            if (!$payload = $this->jwtGuard->decode($token)) {
                throw new AuthenticationException('Authentication failed.');
            }

            $account = $this->retrieveAccount($payload);

            if (!$this->jwtGuard->validate($token, $account, $payload)) {
                throw new AuthenticationException('Authentication failed.');
            }

        } catch (FirewallJwtException $jwtException) {
            throw new AuthenticationException('Authentication failed.');
        }

        $this->checkAccount($account);

        return $this->createAuthenticatedToken($account, $token);
    }

    private function retrieveAccount(Token $token): LocalUser
    {
        if (!$token->hasClaim('email')) {
            throw new AuthenticationException('Jwt token is not valid.');
        }

        return $this->userProvider->requireByIdentifier(
            EmailAddress::fromString($token->getClaim('email'))
        );
    }

    private function createAuthenticatedToken(LocalUser $account, Tokenable $token): Tokenable
    {
        $authenticatedToken = new JwtApiToken($this->providerKey, $token->credentials(), $account->getRoles());
        $authenticatedToken->setUser(
            UserToken::withUser($account)
        );

        return $authenticatedToken;
    }

    private function checkAccount(LocalUser $account): void
    {
        $this->userChecker->checkPreAuthentication($account);

        $this->userChecker->checkPostAuthentication($account);
    }

    public function supports(Tokenable $token): bool
    {
        return $token instanceof JwtApiToken
            && $this->providerKey->sameValueAs($token->providerKey());
    }
}