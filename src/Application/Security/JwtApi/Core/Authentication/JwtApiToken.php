<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\JwtApi\Core\Authentication;

use Hewsda\Security\Core\Authentication\Token\Token;
use Hewsda\Security\Foundation\Value\ProviderKey;

class JwtApiToken extends Token
{
    /**
     * @var string string
     */
    private $credentials;

    /**
     * @var ProviderKey
     */
    private $providerKey;

    /**
     * JwtApiToken constructor.
     *
     * @param ProviderKey $providerKey
     * @param string $rawToken
     * @param array $roles
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(ProviderKey $providerKey, string $rawToken, array $roles = [])
    {
        parent::__construct($roles);

        $this->credentials = $rawToken;
        $this->providerKey = $providerKey;

        $this->setAuthenticated(count($roles) > 0);
    }

    public function credentials(): string
    {
        return $this->credentials;
    }

    public function providerKey(): ProviderKey
    {
        return $this->providerKey;
    }
}