<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\JwtApi\Http\Request;

use Hewsda\Security\Foundation\Contracts\Http\Authentication\AuthenticationRequest;
use Illuminate\Http\Request as IlluminateRequest;
use Symfony\Component\HttpFoundation\Request;

class JwtApiLoginAuthenticationRequest implements AuthenticationRequest
{

    public function extract(IlluminateRequest $request): array
    {
        if (!$this->matches($request)) {
            return [null, null];
        }

        return [$request->input('identifier'), $request->input('password')];
    }

    public function matches(Request $request)
    {
        return $request->is('api/login')
            && $request->acceptsJson()
            && $request->isMethod('post');
    }
}