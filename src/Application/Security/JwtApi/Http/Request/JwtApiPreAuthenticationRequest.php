<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\JwtApi\Http\Request;

use Hewsda\Security\Foundation\Contracts\Http\Authentication\AuthenticationRequest;
use Illuminate\Http\Request as IlluminateRequest;
use Symfony\Component\HttpFoundation\Request;

class JwtApiPreAuthenticationRequest implements AuthenticationRequest
{

    public function extract(IlluminateRequest $request): ?string
    {
        if (!$this->matches($request)) {
            return null;
        }

        $headerParts = explode(' ', $request->headers->get('Authorization'));
        if (!(count($headerParts) === 2 && $headerParts[0] === 'Bearer')) {
            return null;
        }

        return $headerParts[1];
    }

    public function matches(Request $request): bool
    {
        if (!$request->is('*api*')) {
            return false;
        }

        if (!$request->headers->has('Authorization')) {
            return false;
        }

        return true;
    }
}