<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\JwtApi\Http\Authorization;

use Hewsda\Security\Core\Exception\AccessDeniedException;
use Hewsda\Security\Foundation\Contracts\Http\Authorization\AccessDenied;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class JsonApiDeniedHandler implements AccessDenied
{

    public function handle(Request $request, AccessDeniedException $accessDeniedException): Response
    {
        $status = Response::HTTP_UNAUTHORIZED;

        return new JsonResponse([
            'message' => 'Insufficient privileges.',
            'status' => $status
        ], $status);
    }
}