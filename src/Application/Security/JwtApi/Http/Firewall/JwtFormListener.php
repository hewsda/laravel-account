<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\JwtApi\Http\Firewall;

use Hewsda\Security\Core\Authentication\Token\GenericFormToken;
use Hewsda\Security\Core\Exception\AuthenticationException;
use Hewsda\Security\Core\Exception\BadCredentials;
use Hewsda\Security\Foundation\Contracts\Http\Authentication\AuthenticationFailure;
use Hewsda\Security\Foundation\Contracts\Http\Authentication\AuthenticationRequest;
use Hewsda\Security\Foundation\Value\ProviderKey;
use Hewsda\Security\Http\Firewall\AbstractAuthenticationListener;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class JwtFormListener extends AbstractAuthenticationListener
{
    /**
     * @var ProviderKey
     */
    private $providerKey;

    /**
     * @var AuthenticationFailure
     */
    private $failureHandler;

    /**
     * @var AuthenticationRequest
     */
    private $authenticationRequest;

    /**
     * GenericFormListener constructor.
     *
     * @param ProviderKey $providerKey
     * @param AuthenticationFailure $failureHandler
     * @param AuthenticationRequest $authenticationRequest
     */
    public function __construct(ProviderKey $providerKey,
                                AuthenticationFailure $failureHandler,
                                AuthenticationRequest $authenticationRequest)
    {
        $this->providerKey = $providerKey;
        $this->failureHandler = $failureHandler;
        $this->authenticationRequest = $authenticationRequest;
    }

    protected function isRequired(Request $request): bool
    {
        return $this->authenticationRequest->matches($request);
    }

    protected function processAuthentication(Request $request): ?Response
    {
        [$identifier, $password] = $this->extractCredentialsFromRequest($request);

        try {
            $token = $this->authenticate($this->createToken($identifier, $password));

            $this->setToken($token);

            // we could also return a response but we could not naturally
            // authorize a user so we let the flow keep on and handle the response
            // in a controller
            return null;
        } catch (AuthenticationException $exception) {
            return $this->onFailure($request, $exception);
        }
    }

    protected function onFailure(Request $request, AuthenticationException $exception): Response
    {
        if ($this->token() instanceof GenericFormToken &&
            $this->providerKey->sameValueAs($this->token()->providerKey())
        ) {
            $this->eraseStorage();
        }

        return $this->failureHandler->onAuthenticationFailure($request, $exception);
    }

    protected function extractCredentialsFromRequest(Request $request): array
    {
        $credentials = $this->authenticationRequest->extract($request);

        if (!$credentials || !$credentials[0] || !$credentials[1]) {
            throw new BadCredentials('No credentials found in request.');
        }

        return $credentials;
    }

    protected function createToken(string $identifier, string $password): GenericFormToken
    {
        return new GenericFormToken($identifier, $password, $this->providerKey);
    }
}