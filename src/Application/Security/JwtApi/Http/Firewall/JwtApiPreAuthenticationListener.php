<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\JwtApi\Http\Firewall;

use Hewsda\Account\Application\Security\JwtApi\Core\Authentication\JwtApiToken;
use Hewsda\Security\Core\Exception\AuthenticationException;
use Hewsda\Security\Core\Exception\BadCredentials;
use Hewsda\Security\Foundation\Contracts\Core\Authentication\TrustResolver;
use Hewsda\Security\Foundation\Contracts\Http\Authentication\AuthenticationFailure;
use Hewsda\Security\Foundation\Contracts\Http\Authentication\AuthenticationRequest;
use Hewsda\Security\Foundation\Value\ProviderKey;
use Hewsda\Security\Http\Firewall\Firewall;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class JwtApiPreAuthenticationListener extends Firewall
{
    /**
     * @var ProviderKey
     */
    private $providerKey;
    /**
     * @var AuthenticationRequest
     */
    private $authenticationRequest;

    /**
     * @var AuthenticationFailure
     */
    private $authenticationFailure;

    /**
     * @var TrustResolver
     */
    private $trustResolver;

    /**
     * JwtPreAuthenticationListener constructor.
     *
     * @param ProviderKey $providerKey
     * @param AuthenticationRequest $authenticationRequest
     * @param AuthenticationFailure $authenticationFailure
     * @param TrustResolver $trustResolver
     */
    public function __construct(ProviderKey $providerKey,
                                AuthenticationRequest $authenticationRequest,
                                AuthenticationFailure $authenticationFailure,
                                TrustResolver $trustResolver)
    {
        $this->providerKey = $providerKey;
        $this->authenticationRequest = $authenticationRequest;
        $this->authenticationFailure = $authenticationFailure;
        $this->trustResolver = $trustResolver;
    }

    protected function processAuthentication(Request $request): ?Response
    {
        try {
            $authenticatedToken = $this->authenticate($this->createToken($request));
            $this->setToken($authenticatedToken);

            return null;
        } catch (AuthenticationException $exception) {
            return $this->authenticationFailure->onAuthenticationFailure($request, $exception);
        }
    }

    protected function createToken(Request $request): JwtApiToken
    {
        if (!$payload = $this->authenticationRequest->extract($request)) {
            throw new BadCredentials('Invalid credentials.');
        }

        return new JwtApiToken($this->providerKey, $payload);
    }

    protected function isRequired(Request $request): bool
    {
        if (null !== $this->token() && !$this->trustResolver->isAnonymous($this->token())) {
            return false;
        }

        return $this->authenticationRequest->matches($request);
    }
}