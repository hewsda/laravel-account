<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\JwtApi\Http\Authentication;

use Hewsda\Security\Core\Exception\AuthenticationException;
use Hewsda\Security\Foundation\Contracts\Http\Authentication\AuthenticationFailure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class JwtApiFailureHandler implements AuthenticationFailure
{

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
    {
        $status = Response::HTTP_UNAUTHORIZED;

        return new JsonResponse([
            'message' => 'Authentication failure',
            'internal' => $exception->getMessage(),
            'status' => $status
        ], $status);
    }
}