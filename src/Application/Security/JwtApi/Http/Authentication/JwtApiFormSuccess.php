<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\JwtApi\Http\Authentication;

use Hewsda\Account\Application\Security\JwtApi\JwtGuard\JwtGuard;
use Hewsda\Security\Foundation\Contracts\Core\Authentication\Token\Tokenable;
use Hewsda\Security\Foundation\Contracts\Http\Authentication\AuthenticationSuccess;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class JwtApiFormSuccess implements AuthenticationSuccess
{
    /**
     * @var JwtGuard
     */
    private $jwtGuard;

    /**
     * JwtApiFormSuccess constructor.
     *
     * @param JwtGuard $jwtGuard
     */
    public function __construct(JwtGuard $jwtGuard)
    {
        $this->jwtGuard = $jwtGuard;
    }

    public function onAuthenticationSuccess(Request $request, Tokenable $token): Response
    {
        $jwtTokenString = $this->jwtGuard->encode($token, $request);
        $status = Response::HTTP_FOUND;

        return new JsonResponse([
            'message' => 'login successful',
            'status' => $status,
            'token' => (string)$jwtTokenString
        ], $status);
    }
}