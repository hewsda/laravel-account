<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\FormLogin;

use Hewsda\Firewall\Factory\Payload\PayloadService;
use Hewsda\Firewall\Factory\Services\FormAuthenticationFactory;
use Hewsda\Security\Http\Firewall\GenericFormListener;
use Illuminate\Contracts\Foundation\Application;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;

class FormLoginAuthenticationFactory extends FormAuthenticationFactory
{
    public function registerListener(PayloadService $payload): string
    {
        $id = 'firewall.' . $this->position() . '.' . $this->key() . '_authentication_listener.' . $payload->firewallId();

        $this->container->bindIf($id, function (Application $app) use ($payload) {
            return new GenericFormListener(
                $payload->firewallKey,
                $app->make($this->registerAuthenticationSuccess()),
                $app->make($this->registerAuthenticationFailure()),
                $this->matcher(),
                $payload->context->isStateless()
            );
        });

        return $id;
    }

    public function position(): string
    {
        return 'form'; //fixMe move to base form factory
    }

    public function key(): string
    {
        return 'form-login';
    }

    public function matcher(): ?RequestMatcherInterface
    {
        return new FormLoginAuthenticationRequest('front.login.post');
    }

    public function before(): array
    {
        return [];
    }

    public function after(): array
    {
        return [];
    }
}