<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\FormLogin;

use Hewsda\Security\Foundation\Contracts\Http\Authentication\AuthenticationRequest;
use Illuminate\Http\Request as IlluminateRequest;
use Symfony\Component\HttpFoundation\Request;

class FormLoginAuthenticationRequest implements AuthenticationRequest
{
    /**
     * @var string
     */
    private $routeName;

    /**
     * FormLoginAuthenticationRequest constructor.
     *
     * @param string $routeName
     */
    public function __construct(string $routeName)
    {
        $this->routeName = $routeName;
    }

    public function extract(IlluminateRequest $request): array
    {
        if ($this->matches($request)) {
            return FormLoginCredentials::fromRequest($request)->toArray();
        }

        return [null, null];
    }

    public function matches(Request $request): bool
    {
        return $this->routeName === $request->route()->getName();
    }
}