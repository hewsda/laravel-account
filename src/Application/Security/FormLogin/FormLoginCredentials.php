<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\FormLogin;

use Hewsda\Account\Application\Values\Account\AccountEmail;
use Hewsda\Account\Application\Values\Account\LocalPassword;
use Hewsda\Account\Application\Values\Account\Password;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;

class FormLoginCredentials implements Arrayable
{

    /**
     * @var AccountEmail
     */
    private $email;

    /**
     * @var Password
     */
    private $password;

    /**
     * FormLoginCredentials constructor.
     *
     * @param AccountEmail $email
     * @param Password $password
     */
    private function __construct(AccountEmail $email, Password $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    public static function fromRequest(Request $request): FormLoginCredentials
    {
        return self::fromValues($request->input('identifier'), $request->input('password'));
    }

    public static function fromValues($identifier, $password): FormLoginCredentials
    {
        return new self(AccountEmail::fromString($identifier), LocalPassword::fromString($password));
    }

    public function email(): string
    {
        return $this->email->toString();
    }

    public function password(): string
    {
        return $this->password->toString();
    }

    public function toArray(): array
    {
        return [
            $this->email(),
            $this->password()
        ];
    }
}