<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\Logout;

use Hewsda\Security\Foundation\Contracts\Http\Authentication\AuthenticationRequest;
use Illuminate\Http\Request as IlluminateRequest;
use Symfony\Component\HttpFoundation\Request;

class LogoutAuthenticationRequest implements AuthenticationRequest
{

    public function extract(IlluminateRequest $request): void
    {
    }

    public function matches(Request $request): bool
    {
        return 'front.logout' === $request->route()->getName();
    }
}