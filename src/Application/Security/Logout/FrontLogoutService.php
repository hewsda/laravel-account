<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\Security\Logout;

use Hewsda\Firewall\Foundation\Logout\BaseLogoutService;
use Hewsda\Security\Http\Logout\DefaultLogoutSuccessHandler;
use Hewsda\Security\Http\Logout\SessionLogoutHandler;

class FrontLogoutService extends BaseLogoutService
{
    public function logoutHandlers(): ?array
    {
        return array_merge($this->logoutHandlers, [SessionLogoutHandler::class]);
    }

    public function successHandler(): ?string
    {
        return DefaultLogoutSuccessHandler::class;
    }

    public function authenticationRequest(): ?string
    {
        return LogoutAuthenticationRequest::class;
    }
}