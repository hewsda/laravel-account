<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\ServiceBus\Backend;

use Prooph\ServiceBus\CommandBus;

class BackendCommandBus extends CommandBus
{
}