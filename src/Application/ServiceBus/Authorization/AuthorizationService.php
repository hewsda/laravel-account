<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\ServiceBus\Authorization;

use Hewsda\Security\Foundation\Authorizer;
use Hewsda\Security\Foundation\HasToken;
use Prooph\Common\Messaging\Message;
use Prooph\ServiceBus\Async\AsyncMessage;
use Prooph\ServiceBus\Plugin\Guard\AuthorizationService as ProophGuard;

class AuthorizationService implements ProophGuard
{
    use HasToken, Authorizer;

    public function isGranted(string $messageName, $context = null): bool
    {
        if ($this->hasGrantContext($context)) {
            return (boolean)$context->metadata()['grant'];
        }

        return $this->grant($this->requireToken(), [$messageName], $context);
    }

    private function hasGrantContext($context = null): bool
    {
        if ($context && !$context instanceof AsyncMessage && $this->hasGrantMetadata($context)) {
            return (boolean)$context->metadata()['grant'];
        }

        return false;
    }

    private function hasGrantMetadata(Message $message): bool
    {
        return isset($message->metadata()['grant']);
    }
}