<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\ServiceBus\Authorization;

use Hewsda\Security\Core\Autorization\Voter\Voter;
use Hewsda\Security\Foundation\Contracts\Core\Authentication\Token\Tokenable;

class FQCNMessageNameVoter extends Voter
{

    protected function voteOnAttribute(string $attribute, $subject, Tokenable $token): bool
    {
        logger('message name:' . $attribute);
        return true;
    }

    protected function supports(string $attribute, $subject): bool
    {
        return class_exists($attribute);
    }
}