<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\ServiceBus\Async;

use Prooph\ServiceBus\CommandBus;

class AsyncAccountCommandBus extends CommandBus
{
}