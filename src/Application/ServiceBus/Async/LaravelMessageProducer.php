<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\ServiceBus\Async;

use Illuminate\Contracts\Bus\Dispatcher as IlluminateBus;
use Prooph\Common\Messaging\Message;
use Prooph\Common\Messaging\MessageConverter;
use Prooph\ServiceBus\Async\MessageProducer;
use React\Promise\Deferred;

class LaravelMessageProducer implements MessageProducer
{
    /**
     * @var IlluminateBus
     */
    private $dispatcher;

    /**
     * @var MessageConverter
     */
    private $messageConverter;

    /**
     * LaravelMessageProducer constructor.
     *
     * @param IlluminateBus $dispatcher
     * @param MessageConverter $messageConverter
     */
    public function __construct(IlluminateBus $dispatcher, MessageConverter $messageConverter)
    {
        $this->dispatcher = $dispatcher;
        $this->messageConverter = $messageConverter;
    }

    public function __invoke(Message $message, Deferred $deferred = null): void
    {
        $payload = $this->convertMessage($message);

        if ($deferred) {
            // checkMe handle async result with deferred
            $deferred->resolve(
                $this->dispatcher->dispatchNow(new AsyncProcessMessage($payload))
            );
            return;
        }

        $this->dispatcher->dispatch(new AsyncProcessMessage($payload));
    }

    private function convertMessage(Message $message): array
    {
        $data = $this->messageConverter->convertToArray($message);

        $data['created_at'] = $message->createdAt();

        return $data;
    }
}