<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\ServiceBus\Metadata;

use Illuminate\Http\Request;

class NoopMetadataGatherer implements MetadataGatherer
{

    public function getFromRequest(Request $request): array
    {
        return [];
    }
}