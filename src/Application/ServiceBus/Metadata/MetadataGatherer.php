<?php

declare(strict_types=1);

namespace Hewsda\Account\Application\ServiceBus\Metadata;

use Illuminate\Http\Request;

interface MetadataGatherer
{
    public function getFromRequest(Request $request): array;
}