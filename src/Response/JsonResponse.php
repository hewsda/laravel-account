<?php

declare(strict_types=1);

namespace Hewsda\Account\Response;

use React\Promise\PromiseInterface;
use Symfony\Component\HttpFoundation\Response;

class JsonResponse implements ResponseStrategy
{
    public function fromPromise(PromiseInterface $promise): Response
    {
        $json = null;

        $promise->then(function ($data) use (&$json) {
            $json = $data;
        });

        return new \Illuminate\Http\JsonResponse($json);
    }

    public function withStatus(int $statusCode): Response
    {
        return new \Illuminate\Http\JsonResponse([], $statusCode);
    }
}