<?php

declare(strict_types=1);

namespace Hewsda\Account\ProcessManager;

use Hewsda\Account\Application\ServiceBus\Async\AsyncAccountCommandBus;
use Hewsda\Account\Model\Account\Activation\Event\NotActivatedAccountWasCreated;
use Hewsda\Account\Model\Account\LocalAccount\Command\SendWelcomeMessageMail;

class SendWelcomeMessageProcessManager
{

    /**
     * @var AsyncAccountCommandBus
     */
    private $commandBus;

    /**
     * SendWelcomeRegistrationEmailWithActivationLink constructor.
     *
     * @param AsyncAccountCommandBus $commandBus
     */
    public function __construct(AsyncAccountCommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function onEvent(NotActivatedAccountWasCreated $event): void
    {
        $this->commandBus->dispatch(SendWelcomeMessageMail::withActivationCode(
            $event->accountId()->toString(),
            $event->activationCode()->code()
        ));
    }
}