<?php

declare(strict_types=1);

namespace Hewsda\Account\ProcessManager;

use Hewsda\Account\Application\ServiceBus\Async\AsyncAccountCommandBus;
use Hewsda\Account\Model\Account\LocalAccount\Command\SendPasswordResetLink;
use Hewsda\Account\Model\Account\PasswordReset\Event\PasswordResetWasCreated;

class SendPasswordResetLinkProcessManager
{
    /**
     * @var AsyncAccountCommandBus
     */
    private $commandBus;

    /**
     * SendWelcomeRegistrationEmailWithActivationLink constructor.
     *
     * @param AsyncAccountCommandBus $commandBus
     */
    public function __construct(AsyncAccountCommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function onEvent(PasswordResetWasCreated $event): void
    {
        $this->commandBus->dispatch(SendPasswordResetLink::withCode(
            $event->accountId()->toString(),
            $event->code()->toString()
        ));
    }
}