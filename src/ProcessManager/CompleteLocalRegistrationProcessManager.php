<?php

declare(strict_types=1);

namespace Hewsda\Account\ProcessManager;

use Hewsda\Account\Application\Security\AccountActivation\ActivationCode;
use Hewsda\Account\Application\ServiceBus\Async\AsyncAccountCommandBus;
use Hewsda\Account\Model\Account\LocalAccount\Command\AttachRole;
use Hewsda\Account\Model\Account\LocalAccount\Command\MarkAccountAsNotActivated;
use Hewsda\Account\Model\Account\LocalAccount\Event\LocalAccountWasRegistered;

class CompleteLocalRegistrationProcessManager
{
    /**
     * @var AsyncAccountCommandBus
     */
    private $commandBus;

    /**
     * CompleteLocalRegistrationProcess constructor.
     *
     * @param AsyncAccountCommandBus $commandBus
     */
    public function __construct(AsyncAccountCommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function onEvent(LocalAccountWasRegistered $command): void
    {
        $accountId = $command->accountId()->toString();

        $this->commandBus->dispatch(AttachRole::forAccount($accountId, 'ROLE_USER'));

        $this->commandBus->dispatch(MarkAccountAsNotActivated::forAccount(
            $accountId, ActivationCode::nextToken()->code()));
    }
}