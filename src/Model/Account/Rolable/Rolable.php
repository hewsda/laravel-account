<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\Rolable;

use Hewsda\Account\Model\Account\LocalAccount\Event\RoleWasAttached;
use Hewsda\Account\Model\Account\LocalAccount\Event\RoleWasGiven;
use Hewsda\Account\Model\Role\Role;
use Hewsda\LaravelModelEvent\ModelRoot;
use Hewsda\Security\Foundation\Contracts\Entity;
use Hewsda\Security\Foundation\Value\UserId;
use Ramsey\Uuid\Uuid;

class Rolable extends ModelRoot
{

    public static function giveRole(UserId $userId, Role $role, RoleAssigner $roleAssigner = null): Rolable
    {
        $self = new self();

        if ($roleAssigner) {
            $self->recordThat(RoleWasGiven::forAccount($userId, $role->getId(), $roleAssigner));
        } else {
            $self->recordThat(RoleWasAttached::forAccount($userId, $role->getId()));
        }

        return $self;
    }

    public function detachRole(): void
    {

    }

    public function whenRoleWasAttached(RoleWasAttached $event): void
    {
        $this->id = $event->aggregateId();
        $this->accountId = $event->accountId();
        $this->roleId = $event->roleId();
    }

    public function whenRoleWasGiven(RoleWasGiven $event): void
    {
        $this->id = $event->aggregateId();
        $this->accountId = $event->accountId();
        $this->roleId = $event->roleId();
        $this->assigner_id = $event->assignerId();
    }

    public function sameIdentityAs(Entity $aEntity): bool
    {
        return $aEntity instanceof $this &&
            $this->id->equals($aEntity->getId()) &&
            $this->accountId->sameValueAs($aEntity->accountId()) &&
            $this->roleId === $aEntity->roleId();
    }

    protected function aggregateId(): string
    {
        return $this->id->toString();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function accountId(): UserId
    {
        return $this->accountId;
    }

    public function roleId(): string
    {
        return $this->roleId;
    }
}