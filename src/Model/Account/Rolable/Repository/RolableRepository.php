<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\Rolable\Repository;

use Hewsda\Account\Model\Account\Rolable\Rolable;

interface RolableRepository
{
    public function save(Rolable $rolable): void;
}