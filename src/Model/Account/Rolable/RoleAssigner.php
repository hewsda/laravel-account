<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\Rolable;

use Hewsda\Account\Application\Values\Value;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class RoleAssigner implements Value
{

    /**
     * @var UuidInterface
     */
    private $uuid;

    /**
     * RoleAssigner constructor.
     *
     * @param UuidInterface $uuid
     */
    public function __construct(UuidInterface $uuid)
    {
        $this->uuid = $uuid;
    }

    public static function fromString($accountId): self
    {
        return new self(Uuid::fromString($accountId));
    }

    public function assignerId(): UuidInterface
    {
        return $this->uuid;
    }

    public function toString(): string
    {
        return $this->uuid->toString();
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function sameValueAs(\Hewsda\Security\Foundation\Contracts\Value $aValue): bool
    {
        return $aValue instanceof $this && $this->uuid->equals($aValue->assignerId());
    }
}