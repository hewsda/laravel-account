<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Query;

use Hewsda\Account\Application\Exceptions\Assertion;

class GetLocalAccountById
{
    /**
     * @var string
     */
    private $accountId;

    /**
     * GetLocalAccountById constructor.
     *
     * @param $accountId
     */
    public function __construct($accountId)
    {
        $this->accountId = $accountId;
    }

    public function accountId(): string
    {
        Assertion::string($this->accountId);

        return $this->accountId;
    }
}