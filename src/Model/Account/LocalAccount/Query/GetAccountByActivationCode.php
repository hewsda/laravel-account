<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Query;

use Hewsda\Account\Application\Security\AccountActivation\ActivationCode;

class GetAccountByActivationCode
{
    /**
     * @var string
     */
    private $activationCode;

    /**
     * GetAccountByActivationCode constructor.
     *
     * @param $activationCode
     */
    public function __construct($activationCode)
    {
        $this->activationCode = $activationCode;
    }

    public function activationCode(): ActivationCode
    {
        return ActivationCode::fromString($this->activationCode);
    }
}