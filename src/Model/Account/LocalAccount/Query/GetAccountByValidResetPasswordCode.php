<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Query;

use Hewsda\Account\Model\Account\PasswordReset\PasswordResetCode;

class GetAccountByValidResetPasswordCode
{
    /**
     * @var string
     */
    private $code;

    /**
     * GetAccountByValidResetPasswordCode constructor.
     *
     * @param $code
     */
    public function __construct($code)
    {
        $this->code = $code;
    }

    public function code(): PasswordResetCode
    {
        return PasswordResetCode::fromString($this->code);
    }
}