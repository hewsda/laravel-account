<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Services;

use Hewsda\Account\Application\Values\Account\EncodedPassword;
use Hewsda\Account\Application\Values\Account\LocalPassword;

interface LocalPasswordEncoder
{
    public function match(string $clearPassword, EncodedPassword $encodedPassword): bool;

    public function __invoke(LocalPassword $password): EncodedPassword;
}