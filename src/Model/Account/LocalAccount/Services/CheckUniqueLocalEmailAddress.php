<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Services;

use Hewsda\Account\Application\Values\Account\AccountEmail;
use Hewsda\Account\Application\Values\Account\LocalAccountId;

interface CheckUniqueLocalEmailAddress
{
    public function __invoke(AccountEmail $email): ?LocalAccountId;
}