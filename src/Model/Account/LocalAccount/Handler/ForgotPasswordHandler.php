<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Handler;

use Hewsda\Account\Infrastructure\Repository\Account\Local\LocalAccountRepository;
use Hewsda\Account\Infrastructure\Repository\Account\PasswordReset\PasswordResetRepository;
use Hewsda\Account\Model\Account\Exceptions\AccountNotFound;
use Hewsda\Account\Model\Account\LocalAccount\Command\ForgotPassword;

class ForgotPasswordHandler
{
    /**
     * @var LocalAccountRepository
     */
    private $accountRepository;

    /**
     * @var PasswordResetRepository
     */
    private $passwordRepository;

    /**
     * ForgotPasswordHandler constructor.
     *
     * @param LocalAccountRepository $accountRepository
     * @param PasswordResetRepository $passwordRepository
     */
    public function __construct(LocalAccountRepository $accountRepository, PasswordResetRepository $passwordRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->passwordRepository = $passwordRepository;
    }

    public function __invoke(ForgotPassword $command): void
    {
        if (!$account = $this->accountRepository->accountOfEmail($command->email()->toString())) {
            throw new AccountNotFound('Account not found.');
        }

        $this->passwordRepository->save(
            $account->forgotPassword($command->code())
        );
    }
}