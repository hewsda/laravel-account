<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Handler;

use Hewsda\Account\Infrastructure\Repository\Account\Local\LocalAccountRepository;
use Hewsda\Account\Model\Account\Exceptions\AccountAlreadyExists;
use Hewsda\Account\Model\Account\LocalAccount\LocalAccount;
use Hewsda\Account\Model\Account\LocalAccount\Services\CheckUniqueLocalEmailAddress;
use Hewsda\Account\Model\Account\LocalAccount\Services\LocalPasswordEncoder;

class CreateLocalAccountHandler
{
    /**
     * @var CheckUniqueLocalEmailAddress
     */
    private $uniqueEmail;

    /**
     * @var LocalPasswordEncoder
     */
    private $encode;

    /**
     * @var LocalAccountRepository
     */
    private $accountRepository;

    /**
     * CreateLocalAccountHandler constructor.
     *
     * @param CheckUniqueLocalEmailAddress $uniqueEmail
     * @param LocalPasswordEncoder $encode
     * @param LocalAccountRepository $accountRepository
     */
    public function __construct(CheckUniqueLocalEmailAddress $uniqueEmail, LocalPasswordEncoder $encode, LocalAccountRepository $accountRepository)
    {
        $this->uniqueEmail = $uniqueEmail;
        $this->encode = $encode;
        $this->accountRepository = $accountRepository;
    }

    public function __invoke(CreateLocalAccount $command)
    {
        if (null !== ($this->uniqueEmail)($command->api()->email())) {
            throw new AccountAlreadyExists('Account with email already exists.');
        }

        $encodedPassword = ($this->encode)($command->api()->password());

        $account = LocalAccount::createAccount(
            $command->api()->accountId(),
            $command->api()->email(),
            $command->api()->name(),
            $encodedPassword
        );

        $this->accountRepository->save($account);
    }
}