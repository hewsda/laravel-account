<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Handler;

use Hewsda\Account\Application\Mail\PasswordResetLink;
use Hewsda\Account\Infrastructure\Repository\Account\PasswordReset\PasswordResetRepository;
use Hewsda\Account\Model\Account\Exceptions\AccountNotFound;
use Hewsda\Account\Model\Account\LocalAccount\Command\SendPasswordResetLink;

class SendPasswordResetLinkHandler
{
    /**
     * @var PasswordResetRepository
     */
    private $resetRepository;

    /**
     * SendPasswordResetLinkHandler constructor.
     *
     * @param PasswordResetRepository $resetRepository
     */
    public function __construct(PasswordResetRepository $resetRepository)
    {
        $this->resetRepository = $resetRepository;
    }

    public function __invoke(SendPasswordResetLink $command): void
    {
        $code = $command->passwordResetCode();



        if (!$account = $this->resetRepository->accountOfPasswordResetCode($code->toString())) {
            throw new AccountNotFound(
                sprintf('Account not found with password reset code %s', $code->toString())
            );
        }

        \Mail::to($account->getEmail()->toString())->send(new PasswordResetLink($code));
    }
}