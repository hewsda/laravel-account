<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Handler;

use Hewsda\Account\Infrastructure\Repository\Account\Activation\ActivationRepository;
use Hewsda\Account\Infrastructure\Repository\Account\Local\LocalAccountRepository;
use Hewsda\Account\Model\Account\Exceptions\AccountNotFound;
use Hewsda\Account\Model\Account\LocalAccount\Command\ActivateAccount;

class ActivateAccountHandler
{

    /**
     * @var LocalAccountRepository
     */
    private $repository;

    /**
     * ActivateAccountHandler constructor.
     *
     * @param LocalAccountRepository $repository
     */
    public function __construct(LocalAccountRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(ActivateAccount $command)
    {
        $account = $this->repository->accountOfActivationCode(
            $command->activationCode()->code());

        if (!$account) {
            throw new AccountNotFound('Activation code is no longer valid.');
        }

        $account->markAsActivated();

        $this->repository->save($account);
    }
}