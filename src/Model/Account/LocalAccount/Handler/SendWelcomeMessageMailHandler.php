<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Handler;

use Hewsda\Account\Application\Mail\WelcomeRegistrationWithActivationLink;
use Hewsda\Account\Infrastructure\Repository\Account\Local\LocalAccountRepository;
use Hewsda\Account\Model\Account\Exceptions\AccountNotFound;
use Hewsda\Account\Model\Account\LocalAccount\Command\SendWelcomeMessageMail;

class SendWelcomeMessageMailHandler
{

    /**
     * @var LocalAccountRepository
     */
    private $accountRepository;

    /**
     * SendWelcomeMessageMailHandler constructor.
     *
     * @param LocalAccountRepository $accountRepository
     */
    public function __construct(LocalAccountRepository $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }

    public function __invoke(SendWelcomeMessageMail $command): void
    {
        if (!$account = $this->accountRepository->accountOfActivationCode($command->activationCode()->code())) {
            throw new AccountNotFound(
                sprintf('Account not found with activation code %s', $command->activationCode()->code())
            );
        }

        \Mail::to($account->getEmail()->toString())
            ->send(new WelcomeRegistrationWithActivationLink(
                $account->getDisplayableName(), $command->activationCode()));
    }
}