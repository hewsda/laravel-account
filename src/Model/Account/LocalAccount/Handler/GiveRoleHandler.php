<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Handler;

use Hewsda\Account\Infrastructure\Repository\Account\Local\LocalAccountRepository;
use Hewsda\Account\Infrastructure\Repository\Account\Rolable\RolableRepository;
use Hewsda\Account\Model\Account\Exceptions\AccountNotFound;
use Hewsda\Account\Model\Account\LocalAccount\Command\GiveRole;
use Hewsda\Account\Model\Role\Exception\RoleNotFound;
use Hewsda\Account\Model\Role\Repository\RoleList;

class GiveRoleHandler
{
    /**
     * @var LocalAccountRepository
     */
    private $accountRepository;

    /**
     * @var RoleList
     */
    private $roleCollection;

    /**
     * @var RolableRepository
     */
    private $rolableRepository;

    /**
     * GiveRoleHandler constructor.
     *
     * @param LocalAccountRepository $accountRepository
     * @param RolableRepository $rolableRepository
     * @param RoleList $roleCollection
     */
    public function __construct(LocalAccountRepository $accountRepository, RolableRepository $rolableRepository, RoleList $roleCollection)
    {
        $this->accountRepository = $accountRepository;
        $this->roleCollection = $roleCollection;
        $this->rolableRepository = $rolableRepository;
    }

    public function __invoke(GiveRole $command)
    {
        if (!$account = $this->accountRepository->accountOfId($command->accountId()->toString())) {
            throw new AccountNotFound(
                sprintf('Account not found with id %s', $command->accountId()->toString())
            );
        }

        if (!$role = $this->roleCollection->roleOfName($command->role())) {
            throw new RoleNotFound(
                sprintf('Role with name not found %s', $command->role())
            );
        }

        if (!$this->accountRepository->accountOfId($command->assignerId()->toString())) {
            throw new AccountNotFound(
                sprintf('Assigner account not found with id %s', $command->assignerId()->toString())
            );
        }

        $rolable = $account->giveRole($role, $command->assignerId());
        if ($rolable) {
            $this->rolableRepository->save($rolable);
        }
    }
}