<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Handler;

use Hewsda\Account\Infrastructure\Repository\Account\Activation\ActivationRepository;
use Hewsda\Account\Infrastructure\Repository\Account\Local\LocalAccountRepository;
use Hewsda\Account\Model\Account\LocalAccount\Command\MarkAccountAsNotActivated;

class MarkAccountAsNotActivatedHandler
{
    /**
     * @var LocalAccountRepository
     */
    private $accountRepository;

    /**
     * @var ActivationRepository
     */
    private $activationRepository;

    /**
     * MarkAccountAsNotActivatedHandler constructor.
     *
     * @param LocalAccountRepository $repository
     * @param ActivationRepository $activationRepository
     */
    public function __construct(LocalAccountRepository $repository, ActivationRepository $activationRepository)
    {
        $this->accountRepository = $repository;
        $this->activationRepository = $activationRepository;
    }

    public function __invoke(MarkAccountAsNotActivated $command): void
    {
        $account = $this->accountRepository->accountOfId($command->accountId()->toString());

        $this->activationRepository->save(
            $account->markAsNotActivated($command->activationCode())
        );
    }
}