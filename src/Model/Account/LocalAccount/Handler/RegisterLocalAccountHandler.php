<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Handler;

use Hewsda\Account\Model\Account\Exceptions\AccountAlreadyExists;
use Hewsda\Account\Model\Account\LocalAccount\Command\RegisterLocalAccount;
use Hewsda\Account\Model\Account\LocalAccount\LocalAccount;
use Hewsda\Account\Model\Account\LocalAccount\Repository\LocalAccountRepositoryContract;
use Hewsda\Account\Model\Account\LocalAccount\Services\CheckUniqueLocalEmailAddress;
use Hewsda\Account\Model\Account\LocalAccount\Services\LocalPasswordEncoder;

class RegisterLocalAccountHandler
{
    /**
     * @var LocalAccountRepositoryContract
     */
    private $repository;

    /**
     * @var CheckUniqueLocalEmailAddress
     */
    private $uniqueEmail;

    /**
     * @var LocalPasswordEncoder
     */
    private $encode;

    /**
     * RegisterLocalAccountHandler constructor.
     *
     * @param LocalAccountRepositoryContract $repository
     * @param CheckUniqueLocalEmailAddress $uniqueEmail
     * @param LocalPasswordEncoder $encoder
     */
    public function __construct(LocalAccountRepositoryContract $repository,
                                CheckUniqueLocalEmailAddress $uniqueEmail,
                                LocalPasswordEncoder $encoder)
    {
        $this->repository = $repository;
        $this->uniqueEmail = $uniqueEmail;
        $this->encode = $encoder;
    }

    public function __invoke(RegisterLocalAccount $command)
    {
        if (null !== ($this->uniqueEmail)($command->form()->email())) {
            throw new AccountAlreadyExists('Account with email already exists.');
        }

        $encodedPassword = ($this->encode)($command->form()->password());

        $account = LocalAccount::register(
            $command->form()->accountId(),
            $command->form()->email(),
            $command->form()->name(),
            $encodedPassword
        );

        $this->repository->save($account);
    }
}