<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Handler;

use Hewsda\Account\Infrastructure\Repository\Account\Local\LocalAccountRepository;
use Hewsda\Account\Model\Account\LocalAccount\Query\GetLocalAccountById;
use React\Promise\Deferred;

class GetLocalAccountByIdHandler
{
    /**
     * @var LocalAccountRepository
     */
    private $accountRepository;

    /**
     * GetAccountByIdHandler constructor.
     *
     * @param LocalAccountRepository $accountRepository
     */
    public function __construct(LocalAccountRepository $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }

    public function __invoke(GetLocalAccountById $query, Deferred $deferred = null)
    {
        $account = $this->accountRepository->accountOfId($query->accountId());

        if (!$deferred) {
            return $account;
        }

        $deferred->resolve($account);
    }
}