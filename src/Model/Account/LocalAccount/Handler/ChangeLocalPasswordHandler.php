<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Handler;

use Hewsda\Account\Infrastructure\Repository\Account\Local\LocalAccountRepository;
use Hewsda\Account\Model\Account\Exceptions\AccountNotFound;
use Hewsda\Account\Model\Account\Exceptions\InvalidCredentials;
use Hewsda\Account\Model\Account\LocalAccount\Command\ChangeLocalPassword;
use Hewsda\Account\Model\Account\LocalAccount\Services\LocalPasswordEncoder;

class ChangeLocalPasswordHandler
{
    /**
     * @var LocalAccountRepository
     */
    private $repository;

    /**
     * @var LocalPasswordEncoder
     */
    private $passwordEncoder;

    /**
     * LocalPasswordChangeHandler constructor.
     *
     * @param LocalAccountRepository $repository
     * @param LocalPasswordEncoder $passwordEncoder
     */
    public function __construct(LocalAccountRepository $repository, LocalPasswordEncoder $passwordEncoder)
    {
        $this->repository = $repository;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function __invoke(ChangeLocalPassword $command)
    {
        $accountId = $command->accountId()->toString();

        if (null === $account = $this->repository->accountOfId($accountId)) {
            throw new AccountNotFound(sprintf('Account not found with id "%s"', $accountId));
        }

        if (!$this->passwordEncoder->match($command->currentPassword(), $account->getPassword())) {
            throw new InvalidCredentials('Current password does not match.');
        }

        if ($command->currentPassword() === $command->localPassword()->toString()) {
            return;
        }

        $password = ($this->passwordEncoder)($command->localPassword());

        $account->changePassword($password);

        $this->repository->save($account);
    }
}