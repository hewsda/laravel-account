<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Handler;

use Hewsda\Account\Infrastructure\Repository\Account\Local\LocalAccountRepository;
use Hewsda\Account\Model\Account\LocalAccount\Query\GetAllAccounts;
use React\Promise\Deferred;

class GetAllAccountsHandler
{
    /**
     * @var LocalAccountRepository
     */
    private $accountRepository;

    public function __construct(LocalAccountRepository $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }

    public function __invoke(GetAllAccounts $query, Deferred $deferred = null)
    {
        $accounts = $this->accountRepository->createModel()->get();

        if (!$deferred) {
            return $accounts;
        }

        $deferred->resolve($accounts);
    }
}