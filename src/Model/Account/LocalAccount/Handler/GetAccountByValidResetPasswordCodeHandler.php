<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Handler;

use Hewsda\Account\Infrastructure\Repository\Account\PasswordReset\PasswordResetRepository;
use Hewsda\Account\Model\Account\Exceptions\AccountNotFound;
use Hewsda\Account\Model\Account\LocalAccount\Query\GetAccountByValidResetPasswordCode;
use React\Promise\Deferred;

class GetAccountByValidResetPasswordCodeHandler
{
    /**
     * @var PasswordResetRepository
     */
    private $resetRepository;

    /**
     * GetAccountByValidResetPasswordCode constructor.
     *
     * @param PasswordResetRepository $resetRepository
     */
    public function __construct(PasswordResetRepository $resetRepository)
    {
        $this->resetRepository = $resetRepository;
    }

    public function __invoke(GetAccountByValidResetPasswordCode $query, Deferred $deferred = null)
    {
        if (!$account = $this->resetRepository->accountOfPasswordResetCode($query->code()->toString())) {
            throw new AccountNotFound(
                sprintf('Password reset link is no longer valid.')
            );
        }

        if (!$deferred) {
            return $account;
        }

        $deferred->resolve($account);
    }
}