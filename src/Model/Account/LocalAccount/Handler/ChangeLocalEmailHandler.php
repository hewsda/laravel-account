<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Handler;

use Hewsda\Account\Model\Account\Exceptions\AccountAlreadyExists;
use Hewsda\Account\Model\Account\LocalAccount\Command\ChangeLocalEmail;
use Hewsda\Account\Model\Account\LocalAccount\Repository\LocalAccountRepositoryContract;
use Hewsda\Account\Model\Account\LocalAccount\Services\CheckUniqueLocalEmailAddress;

class ChangeLocalEmailHandler
{
    /**
     * @var LocalAccountRepositoryContract
     */
    private $repository;

    /**
     * @var CheckUniqueLocalEmailAddress
     */
    private $uniqueEmail;

    /**
     * ChangeLocalEmailAddressHandler constructor.
     *
     * @param LocalAccountRepositoryContract $repository
     * @param CheckUniqueLocalEmailAddress $uniqueEmail
     */
    public function __construct(LocalAccountRepositoryContract $repository, CheckUniqueLocalEmailAddress $uniqueEmail)
    {
        $this->repository = $repository;
        $this->uniqueEmail = $uniqueEmail;
    }

    public function __invoke(ChangeLocalEmail $command)
    {
        if (!$account = $this->repository->accountOfId($command->accountId()->toString())) {
            return;
        }

        if ($account->getEmail()->sameValuesAs($command->email())) {
            return;
        }

        if(null!== ($this->uniqueEmail)($command->email())){
            throw new AccountAlreadyExists('Email address is already registered.');
        }

        $account->changeEmail($command->email());

        $this->repository->save($account);
    }
}