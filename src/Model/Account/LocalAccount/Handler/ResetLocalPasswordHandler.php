<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Handler;

use Hewsda\Account\Infrastructure\Repository\Account\Local\LocalAccountRepository;
use Hewsda\Account\Model\Account\Exceptions\AccountNotFound;
use Hewsda\Account\Model\Account\Exceptions\InvalidCredentials;
use Hewsda\Account\Model\Account\LocalAccount\Command\ResetLocalPassword;
use Hewsda\Account\Model\Account\LocalAccount\Services\LocalPasswordEncoder;
use Hewsda\Account\Model\Account\PasswordReset\Event\PasswordResetWasAttempted;

class ResetLocalPasswordHandler
{

    /**
     * @var LocalAccountRepository
     */
    private $accountRepository;

    /**
     * @var LocalPasswordEncoder
     */
    private $encode;

    /**
     * ResetLocalPasswordHandler constructor.
     *
     * @param LocalAccountRepository $accountRepository
     * @param LocalPasswordEncoder $encode
     */
    public function __construct(LocalAccountRepository $accountRepository, LocalPasswordEncoder $encode)
    {
        $this->accountRepository = $accountRepository;
        $this->encode = $encode;
    }

    public function __invoke(ResetLocalPassword $command): void
    {
        if (!$account = $this->accountRepository->accountOfPasswordResetCode($command->code()->toString())) {
            throw new AccountNotFound('Reset password code no found has been expired.');
        }

        if (!$account->getEmail()->sameValueAs($command->email())) {
            throw new InvalidCredentials('Email address is invalid.');
        }

        $encodedPassword = ($this->encode)($command->localPassword());

        $account->changePassword($encodedPassword);

        $this->accountRepository->save($account);
    }
}