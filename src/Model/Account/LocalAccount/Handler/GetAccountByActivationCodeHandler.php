<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Handler;

use Hewsda\Account\Infrastructure\Repository\Account\Local\LocalAccountRepository;
use Hewsda\Account\Model\Account\LocalAccount\Query\GetAccountByActivationCode;
use React\Promise\Deferred;

class GetAccountByActivationCodeHandler
{
    /**
     * @var LocalAccountRepository
     */
    private $repository;

    /**
     * GetAccountByActivationCodeHandler constructor.
     *
     * @param LocalAccountRepository $repository
     */
    public function __construct(LocalAccountRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(GetAccountByActivationCode $query, Deferred $deferred = null)
    {
        $account = $this->repository->accountOfIdentifier($query->activationCode());

        if (null === $deferred) {
            return $account;
        }

        $deferred->resolve($account);
    }
}