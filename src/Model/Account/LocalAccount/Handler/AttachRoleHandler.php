<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Handler;

use Hewsda\Account\Infrastructure\Repository\Account\Local\LocalAccountRepository;
use Hewsda\Account\Infrastructure\Repository\Account\Rolable\RolableRepository;
use Hewsda\Account\Model\Account\Exceptions\AccountNotFound;
use Hewsda\Account\Model\Account\LocalAccount\Command\AttachRole;
use Hewsda\Account\Model\Role\Exception\RoleNotFound;
use Hewsda\Account\Model\Role\Repository\RoleList;

class AttachRoleHandler
{

    /**
     * @var RolableRepository
     */
    private $rolableRepository;

    /**
     * @var LocalAccountRepository
     */
    private $accountRepository;

    /**
     * @var RoleList
     */
    private $roleCollection;

    /**
     * AttachRoleHandler constructor.
     *
     * @param RolableRepository $rolableRepository
     * @param LocalAccountRepository $accountRepository
     * @param RoleList $roleCollection
     */
    public function __construct(RolableRepository $rolableRepository, LocalAccountRepository $accountRepository, RoleList $roleCollection)
    {
        $this->rolableRepository = $rolableRepository;
        $this->accountRepository = $accountRepository;
        $this->roleCollection = $roleCollection;
    }

    public function __invoke(AttachRole $command): void
    {
        if (!$account = $this->accountRepository->accountOfId($command->accountId()->toString())) {
            throw new AccountNotFound(
                sprintf('Account not found with id %s', $command->accountId()->toString())
            );
        }

        if (!$role = $this->roleCollection->roleOfName($command->roleName())) {
            throw new RoleNotFound(
                sprintf('Role with name not found %s', $command->roleName())
            );
        }

        $this->rolableRepository->save($account->giveRole($role));
    }
}