<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Handler;

use Hewsda\Account\Infrastructure\Repository\Account\Local\LocalAccountRepository;
use Hewsda\Account\Model\Account\Exceptions\AccountNotFound;
use Hewsda\Account\Model\Account\LocalAccount\Command\CreateActivatedAccount;

class CreateActivatedAccountHandler
{
    /**
     * @var LocalAccountRepository
     */
    private $accountRepository;

    /**
     * CreateActivatedAccountHandler constructor.
     *
     * @param LocalAccountRepository $accountRepository
     */
    public function __construct(LocalAccountRepository $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }

    public function __invoke(CreateActivatedAccount $command)
    {
        $accountId = $command->accountId()->toString();

        if (!$account = $this->accountRepository->accountOfId($accountId)) {
            throw new AccountNotFound(
                sprintf('Account not found with id %s', $accountId)
            );
        }

        $account->markAsActivated();

        $this->accountRepository->save($account);
    }
}