<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Values;

use Hewsda\Account\Application\Exceptions\Assertion;
use Hewsda\Account\Application\Values\Account\AccountEmail;
use Hewsda\Account\Application\Values\Account\LocalAccountId;
use Hewsda\Account\Application\Values\Account\LocalPassword;

class BaseAccountRegistration
{
    /**
     * @var array
     */
    private $attributes;

    /**
     * FormAccountRegistration constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes)
    {
        $this->attributes = $attributes;
    }

    public function accountId(): LocalAccountId
    {
        return LocalAccountId::fromString($this->attributes['account_id']);
    }

    public function password(): LocalPassword
    {
        return LocalPassword::fromStringWithConfirmation(
            $this->attributes['password'],
            $this->attributes['password_confirmation']
        );
    }

    public function email(): AccountEmail
    {
        return AccountEmail::fromString($this->attributes['email']);
    }

    public function name(): string
    {
        Assertion::string($this->attributes['name'], 'You must provide a valid name.');

        return $this->attributes['name'];
    }
}