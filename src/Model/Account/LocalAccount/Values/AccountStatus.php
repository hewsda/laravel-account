<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Values;

use Hewsda\Account\Application\Values\Enum;

/**
 * @method static AccountStatus ACTIVATED()
 * @method static AccountStatus NOT_ACTIVATED()
 */
class AccountStatus extends Enum
{
    public const ACTIVATED = 1;
    public const NOT_ACTIVATED = 0;
}