<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Event;

use Hewsda\Account\Application\Values\Account\LocalAccountId;
use Hewsda\Account\Model\Account\LocalAccount\Values\AccountStatus;
use Hewsda\LaravelModelEvent\ModelChanged;

class AccountWasActivated extends ModelChanged
{
    /**
     * @var AccountStatus
     */
    private $activated;

    public static function forAccount(LocalAccountId $accountId, AccountStatus $activated): self
    {
        $self = self::occur($accountId->toString(), [
            'activated' => $activated->getValue()
        ]);

        $self->activated = $activated;

        return $self;
    }

    public function accountId(): LocalAccountId
    {
        return LocalAccountId::fromString($this->aggregateId());
    }

    public function activated(): AccountStatus
    {
        return $this->activated ?? AccountStatus::byValue($this->payload['activated']);
    }
}