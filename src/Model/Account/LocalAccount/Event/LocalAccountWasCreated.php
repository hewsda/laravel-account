<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Event;

use Hewsda\Account\Application\Values\Account\EncodedPassword;
use Hewsda\Account\Application\Values\Account\LocalAccountId;
use Hewsda\LaravelModelEvent\ModelChanged;
use Hewsda\Security\Foundation\Value\EmailAddress;

class LocalAccountWasCreated extends ModelChanged
{
    /**
     * @var $email
     */
    private $email;

    /**
     * @var string
     */
    private $name;

    /**
     * @var EncodedPassword
     */
    private $password;

    public static function withData(LocalAccountId $accountId, EmailAddress $email, string $name, EncodedPassword $password): LocalAccountWasCreated
    {
        $self = self::occur($accountId->toString(), [
            'email' => $email->toString(),
            'name' => $name,
            'password' => $password
        ]);

        $self->email = $email;
        $self->name = $name;
        $self->password = $password;

        return $self;
    }

    public function accountId(): LocalAccountId
    {
        return LocalAccountId::fromString($this->aggregateId());
    }

    public function email(): EmailAddress
    {
        return $this->email ?? EmailAddress::fromString($this->payload['email']);
    }

    public function password(): EncodedPassword
    {
        return $this->password ?? EncodedPassword::fromString($this->payload['password']);
    }

    public function name(): string
    {
        return $this->name ?? $this->payload['name'];
    }
}