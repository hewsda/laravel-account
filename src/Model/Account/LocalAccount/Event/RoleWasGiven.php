<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Event;

use Hewsda\Account\Application\Values\Account\LocalAccountId;
use Hewsda\Account\Model\Account\Rolable\RoleAssigner;
use Hewsda\LaravelModelEvent\ModelChanged;
use Ramsey\Uuid\Uuid;

class RoleWasGiven extends ModelChanged
{
    public static function forAccount(LocalAccountId $userId, string $roleId, RoleAssigner $roleAssigner): RoleWasGiven
    {
        $self = self::occur(Uuid::uuid4()->toString(), [
            'account_id' => $userId->toString(),
            'role_id' => $roleId,
            'assigner_id' => $roleAssigner->toString()
        ]);

        return $self;
    }

    public function accountId(): LocalAccountId
    {
        return LocalAccountId::fromString($this->payload['account_id']);
    }

    public function roleId(): string
    {
        return $this->payload['role_id'];
    }

    public function assignerId(): RoleAssigner
    {
        return RoleAssigner::fromString($this->payload['assigner_id']);
    }
}