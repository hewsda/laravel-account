<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Event;

use Hewsda\Account\Application\Values\Account\EncodedPassword;
use Hewsda\Account\Application\Values\Account\LocalAccountId;
use Hewsda\LaravelModelEvent\ModelChanged;

class LocalPasswordWasChanged extends ModelChanged
{

    /**
     * @var EncodedPassword
     */
    private $newPassword;

    public static function withUser(LocalAccountId $localAccountId, EncodedPassword $newPassword): self
    {
        $self = self::occur($localAccountId->toString(),[
            'new_password' => $newPassword->toString()
        ]);

        $self->newPassword = $newPassword;

        return $self;
    }

    public function accountId(): LocalAccountId
    {
        return LocalAccountId::fromString($this->aggregateId());
    }

    public function newPassword(): EncodedPassword
    {
        return $this->newPassword ?? EncodedPassword::fromString($this->payload['new_password']);
    }
}