<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Event;

use Hewsda\Account\Application\Values\Account\EncodedPassword;
use Hewsda\Account\Application\Values\Account\LocalAccountId;
use Hewsda\Account\Model\Account\PasswordReset\PasswordResetCode;
use Hewsda\LaravelModelEvent\ModelChanged;

class LocalPasswordWasReset extends ModelChanged
{
    /**
     * @var EncodedPassword
     */
    private $newPassword;

    public static function withAccount(LocalAccountId $localAccountId, EncodedPassword $newPassword, PasswordResetCode $code): self
    {
        $self = self::occur($localAccountId->toString(), [
            'new_password' => $newPassword->toString(),
            'code' => $code->toString()
        ]);

        $self->newPassword = $newPassword;
        $self->code = $code;

        return $self;
    }

    public function accountId(): LocalAccountId
    {
        return LocalAccountId::fromString($this->aggregateId());
    }

    public function newPassword(): EncodedPassword
    {
        return $this->newPassword ?? EncodedPassword::fromString($this->payload['new_password']);
    }

    public function code(): PasswordResetCode
    {
        return $this->code ?? PasswordResetCode::fromString($this->payload['code']);
    }
}