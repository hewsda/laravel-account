<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Event;

use Hewsda\Account\Application\Values\Account\AccountEmail;
use Hewsda\Account\Application\Values\Account\LocalAccountId;
use Hewsda\LaravelModelEvent\ModelChanged;

class LocalEmailWasChanged extends ModelChanged
{

    /**
     * @var AccountEmail
     */
    private $newEmail;

    /**
     * @var AccountEmail
     */
    private $currentEmail;

    public static function withAccount(LocalAccountId $accountId, AccountEmail $newEmail, AccountEmail $currentEmail): self
    {
        $self = self::occur($accountId->toString(),[
            'new_email' => $newEmail,
            'current_email' => $currentEmail
        ]);

        $self->newEmail = $newEmail;
        $self->currentEmail = $currentEmail;

        return $self;
    }

    public function accountId(): LocalAccountId
    {
        return LocalAccountId::fromString($this->aggregateId());
    }

    public function newEmail(): AccountEmail
    {
        return $this->newEmail ?? AccountEmail::fromString($this->payload['new_email']);
    }

    public function currentEmail(): AccountEmail
    {
        return $this->currentEmail ?? AccountEmail::fromString($this->payload['current_email']);
    }
}