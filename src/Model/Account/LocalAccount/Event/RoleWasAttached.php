<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Event;

use Hewsda\Account\Application\Values\Account\LocalAccountId;
use Hewsda\LaravelModelEvent\ModelChanged;
use Ramsey\Uuid\Uuid;

class RoleWasAttached extends ModelChanged
{
    public static function forAccount(LocalAccountId $userId, string $roleId): self
    {
        $self = self::occur(Uuid::uuid4()->toString(), [
            'account_id' => $userId->toString(),
            'role_id' => $roleId
        ]);

        return $self;
    }

    public function accountId(): LocalAccountId
    {
        return LocalAccountId::fromString($this->payload['account_id']);
    }

    public function roleId(): string
    {
        return $this->payload['role_id'];
    }
}