<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount;

use Hewsda\Account\Application\Security\AccountActivation\ActivationCode;
use Hewsda\Account\Application\Values\Account\AccountEmail;
use Hewsda\Account\Application\Values\Account\EncodedPassword;
use Hewsda\Account\Application\Values\Account\LocalAccountId;
use Hewsda\Account\Model\Account\Activation\Activation;
use Hewsda\Account\Model\Account\Activation\Contracts\ProvideActivation;
use Hewsda\Account\Model\Account\Exceptions\AccountStatusException;
use Hewsda\Account\Model\Account\LocalAccount\Contracts\LocalAccount as ProvideLocal;
use Hewsda\Account\Model\Account\LocalAccount\Event\AccountWasActivated;
use Hewsda\Account\Model\Account\LocalAccount\Event\LocalAccountWasCreated;
use Hewsda\Account\Model\Account\LocalAccount\Event\LocalAccountWasRegistered;
use Hewsda\Account\Model\Account\LocalAccount\Event\LocalEmailWasChanged;
use Hewsda\Account\Model\Account\LocalAccount\Event\LocalPasswordWasChanged;
use Hewsda\Account\Model\Account\LocalAccount\Event\LocalPasswordWasReset;
use Hewsda\Account\Model\Account\LocalAccount\Values\AccountStatus;
use Hewsda\Account\Model\Account\PasswordReset\PasswordReset;
use Hewsda\Account\Model\Account\PasswordReset\PasswordResetCode;
use Hewsda\Account\Model\Account\Rolable\Rolable;
use Hewsda\Account\Model\Account\Rolable\RoleAssigner;
use Hewsda\Account\Model\Role\Role;
use Hewsda\Security\Foundation\Contracts\Entity;

class LocalAccount extends LocalAccountModel implements Entity, ProvideLocal, ProvideActivation
{
    public static function register(LocalAccountId $accountId, AccountEmail $email, string $name, EncodedPassword $password): self
    {
        $self = new self();
        $self->recordThat(LocalAccountWasRegistered::withData($accountId, $email, $name, $password));

        return $self;
    }

    public static function createAccount(LocalAccountId $accountId, AccountEmail $email, string $name, EncodedPassword $password): self
    {
        $self = new self();
        $self->recordThat(LocalAccountWasCreated::withData($accountId, $email, $name, $password));

        return $self;
    }

    public function giveRole(Role $role, RoleAssigner $roleAssigner = null): ?Rolable
    {
        if ($this->hasRole($role->getName())) {
            return null;
        }

        return Rolable::giveRole($this->getId(), $role, $roleAssigner);
    }

    public function changePassword(EncodedPassword $password): void
    {
        if (!$this->isEnabled()) {
            throw new AccountStatusException('Operation not allowed: Account is not enabled.');
        }

        if ($this->getPassword()->sameValueAs($password)) {
            return;
        }

        $this->recordThat(LocalPasswordWasChanged::withUser($this->getId(), $password));
    }

    public function forgotPassword(PasswordResetCode $code): PasswordReset
    {
        if (!$this->isEnabled()) {
            throw new AccountStatusException('Operation not allowed: Account is not enabled.');
        }

        return PasswordReset::create($this->getId(), $code);
    }

    public function changeEmail(AccountEmail $email): void
    {
        if (!$this->isEnabled()) {
            throw new AccountStatusException('Operation not allowed: Account is not enabled.');
        }

        if ($this->getEmail()->sameValueAs($email)) {
            return;
        }

        $this->recordThat(LocalEmailWasChanged::withAccount($this->getId(), $email, $this->getEmail()));
    }

    public function markAsNotActivated(ActivationCode $code): Activation
    {
        if (!$this->isActivated()) {
            return Activation::createNotActivatedAccount($this->getId(), $code);
        }

        throw new AccountStatusException('Account is not activated already.');
    }

    public function markAsActivated(): void
    {
        if ($this->isActivated()) {
            throw new AccountStatusException('Account is already activated.');
        }

        $this->recordThat(AccountWasActivated::forAccount($this->getId(), AccountStatus::ACTIVATED()));
    }

    public function isEnabled(): bool
    {
        return $this->isActivated();
    }

    public function isActivated(): bool
    {
        return AccountStatus::ACTIVATED()->sameValueAs($this->getActivated());
    }

    public function hasRole(string $roleName): bool
    {
        return in_array($roleName, $this->getRoles(), true);
    }

    public function sameIdentityAs(Entity $aEntity): bool
    {
        return $aEntity instanceof $this && $this->getKey() === $aEntity->getKey();
    }

    public function whenLocalAccountWasRegistered(LocalAccountWasRegistered $event): void
    {
        $this->id = $event->accountId();
        $this->email = $event->email();
        $this->name = $event->name();
        $this->password = $event->password();
    }

    public function whenLocalAccountWasCreated(LocalAccountWasCreated $event): void
    {
        $this->id = $event->accountId();
        $this->email = $event->email();
        $this->name = $event->name();
        $this->password = $event->password();
    }

    public function whenAccountWasActivated(AccountWasActivated $event): void
    {
        $this->activated = $event->activated();
    }

    public function whenLocalPasswordWasChanged(LocalPasswordWasChanged $event): void
    {
        $this->password = $event->newPassword();
    }

    public function whenLocalPasswordWasReset(LocalPasswordWasReset $event): void
    {
        $this->password = $event->newPassword();
    }

    public function whenLocalEmailWasChanged(LocalEmailWasChanged $event): void
    {
        $this->email = $event->newEmail();
    }
}