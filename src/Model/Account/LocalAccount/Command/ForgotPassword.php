<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Command;

use Hewsda\Account\Application\Values\Account\AccountEmail;
use Hewsda\Account\Model\Account\PasswordReset\PasswordResetCode;
use Hewsda\Security\Foundation\Value\EmailAddress;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\ServiceBus\Async\AsyncMessage;

class ForgotPassword extends Command implements AsyncMessage, PayloadConstructable
{
    use PayloadTrait;

    public static function withData($email, $code = null): self
    {
        return new self([
            'email' => $email,
            'code' => $code
        ]);
    }

    public function code(): PasswordResetCode
    {
        if (null === $this->payload['code']) {
            return PasswordResetCode::nextCode();
        }

        return PasswordResetCode::fromString($this->payload['code']);
    }

    public function email(): EmailAddress
    {
        return AccountEmail::fromString($this->payload['email']);
    }
}