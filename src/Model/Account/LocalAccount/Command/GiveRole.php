<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Command;

use Hewsda\Account\Application\Exceptions\Assertion;
use Hewsda\Account\Application\Values\Account\LocalAccountId;
use Hewsda\Account\Model\Account\Rolable\RoleAssigner;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;

class GiveRole extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public static function forAccount($accountId, $assigner_id, $role): GiveRole
    {
        return new self([
            'account_id' => $accountId,
            'assigner_id' => $assigner_id,
            'role' => $role
        ]);
    }

    public function accountId(): LocalAccountId
    {
        return LocalAccountId::fromString($this->payload['account_id']);
    }

    public function assignerId(): RoleAssigner
    {
        return RoleAssigner::fromString($this->payload['assigner_id']);
    }

    public function role(): string
    {
        Assertion::string($this->payload['role']);

        return $this->payload['role'];
    }
}