<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Command;

use Hewsda\Account\Application\Security\AccountActivation\ActivationCode;
use Hewsda\Account\Application\Values\Account\LocalAccountId;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\ServiceBus\Async\AsyncMessage;

class MarkAccountAsNotActivated extends Command implements AsyncMessage, PayloadConstructable
{
    use PayloadTrait;

    public static function forAccount($accountId, $activationCode): self
    {
        return new self(['account_id' => $accountId, 'activation_code' => $activationCode]);
    }

    public function accountId(): LocalAccountId
    {
        return LocalAccountId::fromString($this->payload['account_id']);
    }

    public function activationCode(): ActivationCode
    {
        return ActivationCode::fromString($this->payload['activation_code']);
    }
}