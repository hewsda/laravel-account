<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Command;

use Hewsda\Account\Application\Exceptions\Assertion;
use Hewsda\Account\Application\Values\Account\LocalAccountId;
use Hewsda\Account\Application\Values\Account\LocalPassword;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;

class ChangeLocalPassword extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public static function withData($accountId, $currentPassword, $newPassword, $newPasswordConfirmation): self
    {
        return new self([
            'account_id' => $accountId,
            'current_password' => $currentPassword,
            'new_password' => $newPassword,
            'new_password_confirmation' => $newPasswordConfirmation
        ]);
    }

    public function accountId(): LocalAccountId
    {
        return LocalAccountId::fromString($this->payload['account_id']);
    }

    public function localPassword(): LocalPassword
    {
        return LocalPassword::fromStringWithConfirmation(
            $this->payload['new_password'],
            $this->payload['new_password_confirmation']
        );
    }

    public function currentPassword(): ?string
    {
        $message = 'Your current password is invalid.';

        Assertion::string($this->payload['current_password'], $message);
        Assertion::notEmpty($this->payload['current_password'], $message);

        return $this->payload['current_password'];
    }
}
