<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Command;

use Hewsda\Account\Application\Values\Account\LocalAccountId;
use Hewsda\Account\Model\Account\PasswordReset\PasswordResetCode;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\ServiceBus\Async\AsyncMessage;

class SendPasswordResetLink extends Command implements AsyncMessage, PayloadConstructable
{
    use PayloadTrait;

    public static function withCode($accountId, $code): self
    {
        return new self([
            'account_id' => $accountId,
            'code' => $code
        ]);
    }

    public function accountId(): LocalAccountId
    {
        return LocalAccountId::fromString($this->payload['account_id']);
    }

    public function passwordResetCode(): PasswordResetCode
    {
        return PasswordResetCode::fromString($this->payload['code']);
    }
}