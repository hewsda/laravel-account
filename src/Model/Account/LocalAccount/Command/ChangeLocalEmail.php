<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Command;

use Hewsda\Account\Application\Values\Account\AccountEmail;
use Hewsda\Account\Application\Values\Account\LocalAccountId;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;

class ChangeLocalEmail extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public static function withData($accountId, $email): self
    {
        return new self([
            'account_id' => $accountId,
            'email' => $email
        ]);
    }

    public function accountId(): LocalAccountId
    {
        return LocalAccountId::fromString($this->payload['account_id']);
    }

    public function email(): AccountEmail
    {
        return AccountEmail::fromString($this->payload['email']);
    }
}