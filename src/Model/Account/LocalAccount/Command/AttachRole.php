<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Command;

use Hewsda\Security\Foundation\Value\UserId;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\ServiceBus\Async\AsyncMessage;

class AttachRole extends Command implements AsyncMessage, PayloadConstructable
{
    use PayloadTrait;

    public static function forAccount($accountId, $roleName): self
    {
        return new self([
            'account_id' => $accountId,
            'role_name' => $roleName
        ]);
    }

    public function accountId(): UserId
    {
        return UserId::fromString($this->payload['account_id']);
    }

    public function roleName(): string
    {
        return $this->payload['role_name'];
    }
}