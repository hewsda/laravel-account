<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Command;

use Hewsda\Account\Application\Values\Account\AccountEmail;
use Hewsda\Account\Application\Values\Account\LocalPassword;
use Hewsda\Account\Model\Account\PasswordReset\PasswordResetCode;
use Hewsda\Security\Foundation\Value\EmailAddress;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;

class ResetLocalPassword extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public static function withData($code, $email, $password, $password_confirmation): self
    {
        return new self([
            'code' => $code,
            'email' => $email,
            'new_password' => $password,
            'new_password_confirmation' => $password_confirmation
        ]);
    }

    public function code(): PasswordResetCode
    {
        return PasswordResetCode::fromString($this->payload['code']);
    }

    public function localPassword(): LocalPassword
    {
        return LocalPassword::fromStringWithConfirmation(
            $this->payload['new_password'],
            $this->payload['new_password_confirmation']
        );
    }

    public function email(): EmailAddress
    {
        return AccountEmail::fromString($this->payload['email']);
    }
}