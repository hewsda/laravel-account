<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Handler;

use Hewsda\Account\Model\Account\LocalAccount\Values\BaseAccountRegistration;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;

class CreateLocalAccount extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public function api(): BaseAccountRegistration
    {
        return new BaseAccountRegistration($this->payload());
    }
}