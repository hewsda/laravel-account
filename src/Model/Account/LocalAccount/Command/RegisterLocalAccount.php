<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Command;

use Hewsda\Account\Model\Account\LocalAccount\Values\BaseAccountRegistration;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;

class RegisterLocalAccount extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public static function withData($accountId, $name, $email, $password, $passwordConfirmation): RegisterLocalAccount
    {
        return new self([
            'account_id' => $accountId,
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'password_confirmation' => $passwordConfirmation
        ]);
    }

    public function form(): BaseAccountRegistration
    {
        return new BaseAccountRegistration($this->payload());
    }
}