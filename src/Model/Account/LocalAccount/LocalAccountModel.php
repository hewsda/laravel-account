<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount;

use Hewsda\Account\Application\Values\Account\AccountEmail;
use Hewsda\Account\Application\Values\Account\EncodedPassword;
use Hewsda\Account\Application\Values\Account\LocalAccountId;
use Hewsda\Account\Model\Account\Activation\Activation;
use Hewsda\Account\Model\Account\LocalAccount\Values\AccountStatus;
use Hewsda\Account\Model\Account\PasswordReset\PasswordReset;
use Hewsda\Account\Model\Role\Role;
use Hewsda\LaravelModelEvent\ModelRoot;
use Hewsda\Security\Foundation\Contracts\Core\User\Identifier;
use Hewsda\Security\Foundation\Contracts\Core\User\LocalUser;
use Hewsda\Security\Foundation\Value\EmailAddress;
use Hewsda\Security\Foundation\Value\UserId;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

abstract class LocalAccountModel extends ModelRoot implements LocalUser
{

    /**
     * @var string
     */
    protected $table = 'local_accounts';

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'password', 'activated'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @var array
     */
    protected $with = ['roles'];

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var string
     */
    public $incrementing = false;


    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'account_role', 'account_id')
            ->withPivot(['assigner_id as assigner_id']);
    }

    public function activation(): HasOne
    {
        return $this->hasOne(Activation::class, 'account_id', 'id');
    }

    public function passwordReset(): HasMany
    {
        return $this->hasMany(PasswordReset::class, 'account_id', 'id');
    }

    public function uniquePasswordReset(): hasOne
    {
        return $this->hasOne(PasswordReset::class, 'account_id', 'id');
    }

    public function getId(): UserId
    {
        return LocalAccountId::fromString($this->getKey());
    }

    public function getIdentifier(): Identifier
    {
        return $this->getEmail();
    }

    public function getDisplayableName(): string
    {
        return $this->name;
    }

    public function getEmail(): EmailAddress
    {
        return AccountEmail::fromString($this->email);
    }

    public function getPassword(): EncodedPassword
    {
        return EncodedPassword::fromString($this->password);
    }

    public function getRoles(): array
    {
        return $this->roles->pluck('name')->toArray();
    }

    public function getActivated(): AccountStatus
    {
        return AccountStatus::byValue((int)$this->activated);
    }

    protected function aggregateId(): string
    {
        return $this->getKey();
    }
}