<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\LocalAccount\Repository;

use Hewsda\Account\Model\Account\LocalAccount\LocalAccount;

interface LocalAccountRepositoryContract
{
    public function save(LocalAccount $account): void;
}