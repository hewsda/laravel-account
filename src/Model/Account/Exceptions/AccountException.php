<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\Exceptions;

use Hewsda\Account\Application\Exceptions\AccountDomainException;

class AccountException extends AccountDomainException
{

}