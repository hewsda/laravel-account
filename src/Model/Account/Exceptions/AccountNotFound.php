<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\Exceptions;

class AccountNotFound extends AccountException
{
}