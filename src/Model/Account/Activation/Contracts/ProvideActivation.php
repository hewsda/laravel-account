<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\Activation\Contracts;

use Hewsda\Security\Foundation\Contracts\Core\User\UserActivity;

interface ProvideActivation extends UserActivity
{
}