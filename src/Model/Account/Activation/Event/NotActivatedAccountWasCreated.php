<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\Activation\Event;

use Hewsda\Account\Application\Security\AccountActivation\ActivationCode;
use Hewsda\LaravelModelEvent\ModelChanged;
use Hewsda\Security\Foundation\Value\UserId;
use Ramsey\Uuid\Uuid;

class NotActivatedAccountWasCreated extends ModelChanged
{
    /**
     * @var UserId
     */
    private $accountId;

    /**
     * @var ActivationCode
     */
    private $activationCode;

    public static function forAccount(UserId $accountId, ActivationCode $activationCode): self
    {
        $self = self::occur(Uuid::uuid4()->toString(), [
            'account_id' => $accountId->toString(),
            'activation_code' => $activationCode
        ]);

        $self->accountId = $accountId;
        $self->activationCode = $activationCode;

        return $self;
    }

    public function id(): Uuid
    {
        return Uuid::fromString($this->aggregateId());
    }

    public function accountId(): UserId
    {
        return $this->accountId ?? UserId::fromString($this->payload['account_id']);
    }

    public function activationCode(): ActivationCode
    {
        return $this->activationCode ?? ActivationCode::fromString($this->payload['activation_code']);
    }
}