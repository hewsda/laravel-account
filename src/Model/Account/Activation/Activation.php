<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\Activation;

use Hewsda\Account\Application\Security\AccountActivation\ActivationCode;
use Hewsda\Account\Model\Account\Activation\Event\NotActivatedAccountWasCreated;
use Hewsda\Security\Foundation\Contracts\Entity;
use Hewsda\Security\Foundation\Value\UserId;

class Activation extends BaseActivation implements Entity
{

    public static function createNotActivatedAccount(UserId $accountId, ActivationCode $code): Activation
    {
        $self = new self();
        $self->recordThat(NotActivatedAccountWasCreated::forAccount($accountId, $code));

        return $self;
    }

    public function isValid(): bool
    {
        $date = $this->createdAt()->add(new \DateInterval('P1D'));

        return $date > new \DateTime();
    }

    public function isActivated(): bool
    {
        return false;
    }

    public function whenNotActivatedAccountWasCreated(NotActivatedAccountWasCreated $event): void
    {
        $this->id = $event->id();
        $this->account_id = $event->accountId();
        $this->activation_code = $event->activationCode();
        $this->created_at = $event->createdAt();
    }

    public function sameIdentityAs(Entity $aEntity): bool
    {
        return $aEntity instanceof $this
            && $aEntity->getKey() === $this->getKey();
    }
}