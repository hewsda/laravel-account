<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\Activation;

use Hewsda\Account\Application\Security\AccountActivation\ActivationCode;
use Hewsda\Account\Model\Account\Activation\Contracts\ProvideActivation;
use Hewsda\LaravelModelEvent\ModelRoot;
use Hewsda\Security\Foundation\Value\UserId;

abstract class BaseActivation extends ModelRoot implements ProvideActivation
{
    /**
     * @var string
     */
    protected $table = 'account_activation';

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'account_id', 'activation_code', 'created_at'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

    public function accountId(): UserId
    {
        if (is_string($this->account_id)) {
            return UserId::fromString($this->account_id);
        }

        return $this->accountId;
    }

    public function activationCode(): ActivationCode
    {
        if (is_string($this->activation_code)) {
            return ActivationCode::fromString($this->activation_code);
        }

        return $this->activation_code;
    }

    public function createdAt(): \DateTimeImmutable
    {
        if (is_string($this->created_at)) {
            return new \DateTimeImmutable($this->created_at);
        }

        return $this->created_at;
    }

    protected function aggregateId(): string
    {
        return $this->getKey();
    }
}