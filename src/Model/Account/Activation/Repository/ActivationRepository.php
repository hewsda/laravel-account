<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\Activation\Repository;

use Hewsda\Security\Foundation\Contracts\Core\User\User;

interface ActivationRepository
{
    public function accountOfActivationCode(string $code): ?User;
}