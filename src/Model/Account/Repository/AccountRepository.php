<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\Repository;

use Hewsda\Security\Foundation\Contracts\Core\User\ExtendedUser as SecurityUser;
use Hewsda\Security\Foundation\Contracts\Core\User\UserProvider;

interface AccountRepository extends UserProvider
{
    public function accountOfId(string $userId): ?SecurityUser;

    public function accountOfEmail(string $email): ?SecurityUser;
}