<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\PasswordReset\Repository;

use Hewsda\Security\Foundation\Contracts\Core\User\User;

interface PasswordResetRepository
{
    public function accountOfPasswordResetCode(string $code): ?User;
}