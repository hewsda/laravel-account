<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\PasswordReset;

use Hewsda\LaravelModelEvent\ModelRoot;
use Hewsda\Security\Foundation\Value\UserId;
use Illuminate\Database\Eloquent\Builder;
use Ramsey\Uuid\Uuid;

abstract class PasswordResetModel extends ModelRoot
{

    /**
     * @var string
     */
    protected $table = 'password_resets';

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'account_id', 'code', 'created_at', 'updated_at'
    ];

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var string
     */
    public $incrementing = 'false';

    protected function aggregateId(): string
    {
        return $this->getId()->toString();
    }

    public function scopeIsNotExpired(Builder $q): Builder
    {
        return $q->whereRaw('DATE_ADD(created_at,INTERVAL 1 HOUR) > NOW()');
    }

    public function getId(): Uuid
    {
        if (is_string($this->getKey())) {
            return Uuid::fromString($this->getKey());
        }

        return $this->getKey();
    }

    public function getAccountId(): UserId
    {
        if (is_string($this->account_id)) {
            return UserId::fromString($this->account_id);
        }

        return $this->account_id;
    }

    public function getCode(): PasswordResetCode
    {
        if (is_string($this->code)) {
            return PasswordResetCode::fromString($this->code);
        }

        return $this->code;
    }
}