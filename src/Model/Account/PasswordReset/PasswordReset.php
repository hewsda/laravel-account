<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\PasswordReset;

use Hewsda\Account\Model\Account\PasswordReset\Event\PasswordResetWasCreated;
use Hewsda\Account\Model\Account\PasswordReset\Event\PasswordResetWasDeleted;
use Hewsda\Security\Foundation\Contracts\Entity;
use Hewsda\Security\Foundation\Value\UserId;
use Ramsey\Uuid\Uuid;

class PasswordReset extends PasswordResetModel implements Entity
{

    public static function create(UserId $accountId, PasswordResetCode $code): PasswordReset
    {
        $self = new self();
        $self->recordThat(PasswordResetWasCreated::withData($accountId, $code));

        return $self;
    }

    public function delete(): PasswordReset
    {
        $this->recordThat(PasswordResetWasDeleted::withData(
            $this->getId(), $this->getAccountId(), $this->getCode()));

        return $this;
    }

    public function isValid(): bool
    {
        return !$this->isExpired();
    }

    public function isExpired(): bool
    {
        return $this->createdAt()->add(new \DateInterval('PT1H')) < new \DateTime();
    }

    public function sameIdentityAs(Entity $aEntity): bool
    {
        return $aEntity instanceof $this && $this->getId()->equals($aEntity->getId());
    }

    public function whenPasswordResetWasCreated(PasswordResetWasCreated $event): void
    {
        $this->id = Uuid::fromString($event->aggregateId());
        $this->account_id = $event->accountId();
        $this->code = $event->code();
        $this->created_at = $event->createdAt();
        $this->attempt = 0;
    }

    public function whenPasswordResetWasDeleted(PasswordResetWasDeleted $event): void
    {
    }
}