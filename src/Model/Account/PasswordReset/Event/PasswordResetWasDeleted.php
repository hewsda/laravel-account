<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\PasswordReset\Event;

use Hewsda\Account\Model\Account\PasswordReset\PasswordResetCode;
use Hewsda\LaravelModelEvent\ModelChanged;
use Hewsda\Security\Foundation\Value\UserId;
use Ramsey\Uuid\Uuid;

class PasswordResetWasDeleted extends ModelChanged
{
    public static function withData(Uuid $id, UserId $accountId, PasswordResetCode $code): PasswordResetWasDeleted
    {
        $self = self::occur($id->toString(), [
            'account_id' => $accountId->toString(),
            'code' => $code->toString(),
        ]);

        return $self;
    }

    public function accountId(): UserId
    {
        return $this->payload['account_id'];
    }

    public function code(): PasswordResetCode
    {
        return $this->payload['code'];
    }
}