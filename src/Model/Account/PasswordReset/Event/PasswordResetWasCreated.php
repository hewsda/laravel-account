<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\PasswordReset\Event;

use Hewsda\Account\Model\Account\PasswordReset\PasswordResetCode;
use Hewsda\LaravelModelEvent\ModelChanged;
use Hewsda\Security\Foundation\Value\UserId;
use Ramsey\Uuid\Uuid;

class PasswordResetWasCreated extends ModelChanged
{
    public static function withData(UserId $accountId, PasswordResetCode $code): PasswordResetWasCreated
    {
        $self = self::occur(Uuid::uuid4()->toString(), [
            'account_id' => $accountId->toString(),
            'code' => $code->toString(),
        ]);

        return $self;
    }

    public function accountId(): UserId
    {
        return UserId::fromString($this->payload['account_id']);
    }

    public function code(): PasswordResetCode
    {
        return PasswordResetCode::fromString($this->payload['code']);
    }
}