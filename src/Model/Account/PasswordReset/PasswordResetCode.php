<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Account\PasswordReset;

use Hewsda\Account\Application\Exceptions\Assertion;
use Hewsda\Account\Application\Values\Identification;
use Hewsda\Security\Foundation\Contracts\Value;

class PasswordResetCode implements Value, Identification
{
    const CODE_LENGTH = 60;

    /**
     * @var string
     */
    private $code;

    /**
     * ActivationToken constructor.
     *
     * @param string $code
     */
    private function __construct(string $code)
    {
        $this->code = $code;
    }

    public static function nextCode(): PasswordResetCode
    {
        return new self(self::generateCode());
    }

    public static function fromString($code): PasswordResetCode
    {
        $message = 'Code is not valid.';

        Assertion::string($code, $message);
        Assertion::length($code, self::CODE_LENGTH, $message);

        return new self($code);
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof $this &&
            $this->code === $aValue->toString();
    }

    public function code(): string
    {
        return $this->code;
    }

    public function toString(): string
    {
        return $this->code;
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    private static function generateCode(): string
    {
        return str_random(self::CODE_LENGTH);
    }
}