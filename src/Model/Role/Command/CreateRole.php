<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Role\Command;

use Hewsda\Account\Application\Values\Role\FormRoleCreation;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;

class CreateRole extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public static function withData($name, $description): self
    {
        return new self([
            'name' => $name,
            'description' => $description
        ]);
    }

    public function form(): FormRoleCreation
    {
        return FormRoleCreation::withData($this->payload['name'], $this->payload['description']);
    }
}