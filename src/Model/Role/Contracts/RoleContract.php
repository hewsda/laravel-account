<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Role\Contracts;

use Hewsda\Account\Application\Values\Role\FormRoleCreation;
use Hewsda\Security\Foundation\Contracts\Core\Role\Role as SecurityRole;

interface RoleContract extends SecurityRole
{
}