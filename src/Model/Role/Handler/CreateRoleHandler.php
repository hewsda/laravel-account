<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Role\Handler;

use Hewsda\Account\Model\Role\Command\CreateRole;
use Hewsda\Account\Model\Role\Repository\RoleList;
use Hewsda\Account\Model\Role\Role;

class CreateRoleHandler
{
    /**
     * @var RoleList
     */
    private $roleList;

    /**
     * CreateRoleHandler constructor.
     *
     * @param RoleList $roleList
     */
    public function __construct(RoleList $roleList)
    {
        $this->roleList = $roleList;
    }

    public function __invoke(CreateRole $command): void
    {
        if (!$this->roleList->has($command->form()->role())) {
            $role = Role::createRole($command->form());

            $this->roleList->save($role);
        }
    }
}