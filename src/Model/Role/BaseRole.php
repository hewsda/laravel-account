<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Role;

use Hewsda\Account\Model\Role\Contracts\RoleContract;
use Hewsda\LaravelModelEvent\ModelRoot;

abstract class BaseRole extends ModelRoot implements RoleContract
{
    /**
     * @var string
     */
    protected $table = 'roles';

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'slug', 'description',
    ];

    /**
     * @var bool
     */
    public $incrementing = false;

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getRole(): string
    {
        return $this->name;
    }

    protected function aggregateId(): string
    {
        return $this->getKey();
    }
}