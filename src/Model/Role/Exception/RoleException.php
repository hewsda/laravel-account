<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Role\Exception;

use Hewsda\Account\Application\Exceptions\AccountDomainException;

class RoleException extends AccountDomainException
{
}