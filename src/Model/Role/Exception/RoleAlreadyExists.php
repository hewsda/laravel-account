<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Role\Exception;

class RoleAlreadyExists extends RoleException
{
}