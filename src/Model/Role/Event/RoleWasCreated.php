<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Role\Event;

use Hewsda\LaravelModelEvent\ModelChanged;
use Ramsey\Uuid\Uuid;

class RoleWasCreated extends ModelChanged
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var string
     */
    private $description;

    public static function withData(string $name, string $slug, string $description): self
    {
        $self = self::occur(Uuid::uuid4()->toString(), [
            'name' => $name,
            'slug' => $slug,
            'description' => $description
        ]);

        $self->name = $name;
        $self->slug = $slug;
        $self->description = $description;

        return $self;
    }

    public function name(): string
    {
        return $this->payload['name'];
    }

    public function slug(): string
    {
        return $this->payload['slug'];
    }

    public function description(): string
    {
        return $this->payload['description'];
    }
}