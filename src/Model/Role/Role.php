<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Role;

use Hewsda\Account\Application\Values\Role\FormRoleCreation;
use Hewsda\Account\Model\Role\Contracts\RoleContract;
use Hewsda\Account\Model\Role\Event\RoleWasCreated;
use Hewsda\Security\Foundation\Contracts\Entity;

class Role extends BaseRole implements Entity
{

    public static function createRole(FormRoleCreation $roleCreator): RoleContract
    {
        $self = new static();

        $self->recordThat(RoleWasCreated::withData(
            $roleCreator->role(), $roleCreator->slug(), $roleCreator->description()
        ));

        return $self;
    }

    protected function whenRoleWasCreated(RoleWasCreated $event): void
    {
        $this->id = $event->aggregateId();
        $this->name = $event->name();
        $this->description = $event->description();
        $this->slug = $event->slug();
    }

    public function sameIdentityAs(Entity $aEntity): bool
    {
        return $aEntity instanceof $this && $this->getKey() === $aEntity->getKey();
    }
}