<?php

declare(strict_types=1);

namespace Hewsda\Account\Model\Role\Repository;

use Hewsda\Account\Model\Role\Role;

interface RoleList
{
    public function roleOfId(string $roleId): ?Role;

    public function roleOfName(string $roleName): ?Role;

    public function has(string $roleName): bool;

    public function save(Role $role): void;
}